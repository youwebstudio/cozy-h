<?php
ini_set("soap.wsdl_cache_enabled", "0" ); 
try {
	$client = new SoapClient(
	    "https://gateway.scloud.ru/ecvols_base01/ws/RoistatEvent?wsdl",
	    array(
	        'login' => 'ecvols4roistat',
			'password' => 'Roistat27',
	        'soap_version' => SOAP_1_2,
	        'features' => SOAP_USE_XSI_ARRAY_TYPE, 
	        'trace' => true,
	    )
	);

	$jsonData = json_decode($_REQUEST['data'], true);
	$data = array(
	  'title' => $_REQUEST['title'],
	  'name' => $_REQUEST['name'],	  
	  'phone' => $_REQUEST['phone'],
	  'email' => $_REQUEST['email'],
	  'comment' => $_REQUEST['text'],
	  'visit' => $_REQUEST['visit'],
	  'data' => $_REQUEST['data']
	);

	$result = $client->CreateEvent($data);

	if(isset($result->return)){
		exit(json_encode(array("status" => "ok", "order_id" => $result->return)));
	}

} catch (SoapFault $e) {
	var_dump(constructErrorResponse($e->getMessage()));
}


function constructErrorResponse($message)
{
    $response['error'] = 1;
    $response['message'] = $message;
    return $response;
}
?>