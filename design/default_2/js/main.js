/**
 * Created by dyatel on 01.03.17.
 */
$(function () {
  var data = $.extend({
    'brand' : [],
    'category' : [],
    'feature' : {},
    'price' : []
  }, window.filter_data);

  var initPriceBar = function () {
    var values = $('input[name="filter-prices"]').val().split(',');
    for(var i=0; i < values.length; i++) {
      values[i] = parseInt(values[i]);
    }
    var cur_path = window.location.pathname;
    var tmp = cur_path;
    tmp = tmp.replace("/price/filtry-v-cenovom-diapazone-", "");
    tmp = tmp.replace("-rubley", "");
    arTmp = tmp.split('-');
    // console.log(arTmp);

    if( isInteger(arTmp[0]) && isInteger(arTmp[1]) ) {
      var start = arTmp[0];
      var finish = arTmp[1];
    }
    else {
      var start = values[0];
      var finish = values[values.length-1];
    }

    if(!data.price.length)
      data.price = [start, finish];

    $("#prices-block").find('input[name="start"]').val(start);
    $("#prices-block").find('input[name="finish"]').val(finish);

    function findNearest(includeLeft, includeRight, value) {
      var nearest = null;
      var diff = null;
      for (var i = 0; i < values.length; i++) {
        if ((includeLeft && values[i] <= value) || (includeRight && values[i] >= value)) {
          var newDiff = Math.abs(value - values[i]);
          if (diff == null || newDiff < diff) {
            nearest = values[i];
            diff = newDiff;
          }
        }
      }
      return nearest;
    }

    $("#price-range").slider({
      orientation: 'horizontal',
      range: true,
      min: values[0],
      max: values[values.length-1],
      values: [start, finish],
      slide: function(event, ui) {
        var includeLeft = event.keyCode != $.ui.keyCode.RIGHT;
        var includeRight = event.keyCode != $.ui.keyCode.LEFT;
        var value = findNearest(includeLeft, includeRight, ui.value);
        if (ui.value == ui.values[0]) {
          $(this).slider('values', 0, value);
        }
        else {
          $(this).slider('values', 1, value);
        }
        // $("#price-amount").html('Цена от ' + slider.slider('values', 0) + ' до ' + slider.slider('values', 1) + " рублей" );
        $("#prices-block").find('input[name="start"]').val($(this).slider('values', 0));
        $("#prices-block").find('input[name="finish"]').val($(this).slider('values', 1));
        return false;
      },
      change: function(event, ui) {
        var startPrice = $(this).slider('values', 0);
        var finishPrice = $(this).slider('values', 1);
        if(data.price[0] != startPrice || data.price[1] != finishPrice){
          data.price = [startPrice, finishPrice];
          $(this).trigger('filter');
        }
        // var need_path = "/price/filtry-v-cenovom-diapazone-"+startPrice+"-"+finishPrice+"-rubley";
        // var cur_path = window.location.pathname;
        // console.log(cur_path, need_path);
        // if( cur_path != need_path ) {
        //   window.location.pathname = need_path;
        // }
      }
    });
  };

  initPriceBar();

  $('.js-filter').on('click', function () {
    var $this = $(this);

    $this.toggleClass('active');

    $this.trigger('filter');

    return false;
  });

  $("#prices-block").find('input[name="start"]').on('keyup',function () {
    $("#price-range").slider('values', 0, $(this).val());
  });

  $("#prices-block").find('input[name="finish"]').on('keyup',function () {
    $("#price-range").slider('values', 1, $(this).val());
  });

  $('.js-filter').on('filter', function () {
    try{
      var $this = $(this);
      var _data = $this.data();
      if($this.hasClass('active')){
        switch(_data.type){
          case 'feature':
            data[_data.type][_data.id] = data[_data.type][_data.id] || [];
            data[_data.type][_data.id].push(_data.value);
            break;
          case 'brand':
          case 'category':
            data[_data.type].push(_data.value);
            break;
        }
      } else {
        switch(_data.type){
          case 'feature':
            data[_data.type][_data.id] = data[_data.type][_data.id] || [];
            var index = data[_data.type][_data.id].indexOf(_data.value);
            data[_data.type][_data.id].splice(index, 1);
            if(!data[_data.type][_data.id].length){
              delete data[_data.type][_data.id];
            }
            break;
          case 'brand':
          case 'category':
            var index = data[_data.type].indexOf(_data.value);
            data[_data.type].splice(index, 1);
            break;
        }
      }
      $.ajax({
        type: "GET",
        beforeSend: function(request) {
          request.setRequestHeader("ajax", true);
        },
        url: '/catalog/filter',
        data: data,
        success: function(data) {
          $('#content').html(data);
          window.history.pushState(null, null, this.url.replace('/ajax','').replace('&module=ProductsView&filter=true',''));
        }
      });
    } catch (e){
      console.error(e);
    }
    return false;
  });

  // set values from query-string
  for(var type in data){
    var item = data[type];
    switch(type){
      case 'feature':
        for(var key in item){
          for(var k in item[key]){
            $('.js-filter[data-type="'+type+'"][data-id="'+key+'"][data-value="'+item[key][k]+'"]').addClass('active').parents('.totalBlock').addClass('active');
          }
        }
        break;
      case 'brand':
      case 'category':
        for(var key in item){
          $('.js-filter[data-type="'+type+'"][data-value="'+item[key]+'"]').addClass('active');
        }
        break;
      case 'price':
        $("#price-range").slider('values', 0, item[0]);
        $("#price-range").slider('values', 1, item[1]);
        $("#prices-block").find('input[name="start"]').val(item[0]);
        $("#prices-block").find('input[name="finish"]').val(item[1]);
        break;
    }
  }
});