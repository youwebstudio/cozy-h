{* Страница товара *}

<!-- Хлебные крошки /-->
<div id="path">
	<a href="./">Главная</a>
	{foreach from=$category->path item=cat}
	→ <a href="catalog/{$cat->url}">{$cat->name|escape}</a>
	{/foreach}
	{if $brand}
	→ <a href="catalog/{$cat->url}/{$brand->url}">{$brand->name|escape}</a>
	{/if}
	→  {$product->name|escape}                
</div>
<!-- Хлебные крошки #End /-->

<h1 data-product="{$product->id}">{$product->name|escape}</h1>

<div class="product">

	<p>
		{if (count($product->categories) == 1)}Категория: {else}Категории: {/if}
		{foreach $product->categories as $cat}
			<a href="catalog/{$cat->url}">{$cat->name}</a>
		{/foreach}
	</p>

	<!-- Большое фото -->
	{if $product->image}
	<div class="image">
		<a href="{$product->image->filename|resize:800:600:w}" class="zoom" data-rel="group"><img src="{$product->image->filename|resize:300:300}" alt="{$product->product->name|escape}" /></a>
	</div>
	{/if}
	<!-- Большое фото (The End)-->

	<!-- Описание товара -->
	<div class="description">
	
		{$product->short_descr}
		
		{if $product->variants|count > 0}
		<div class="left-wrap">
		
		<!-- Выбор варианта товара -->
		<form class="variants" action="/cart">
			<table>
			{foreach $product->variants as $v}
			<tr class="variant">
				<td>
					<input id="product_{$v->id}" name="variant" value="{$v->id}" type="radio" class="variant_radiobutton" {if $product->variant->id==$v->id}checked{/if} {if $product->variants|count<2}style="display:none;"{/if}/>
				</td>
				<td>
					{if $v->name}<label class="variant_name" for="product_{$v->id}">{$v->name}</label>{/if}
				</td>
				<td>
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="price">Цена: {$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
				</td>
			</tr>
			{/foreach}
			</table>
			<input type="submit" class="button" value="в корзину" data-result-text="добавлено"/>
				<br/><a class="button1" onClick="modal_state('one_click','block')">Хочу дешевле</a>
		</form>
		
		</div>
		<div class="right-wrap">

			<div class="rating">
			
				<p>Рейтинг: <b class="rating-value">{$product->rating}</b></p>
				<div class="rating-block">
					<div class="rating-stars" style="width: 100%;"></div>
					<div class="rating-active" style="width: {$product->rating*20}%;"></div>
				</div>
			</div>	
		
			<div class="warranty">
				
				<p><b>Гарантия</b><br/>5 лет</p>	
			</div>		

		</div>
		<div style="clear: both;"></div>
		
		<noindex>
			<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
			<script src="//yastatic.net/share2/share.js"></script>
			<div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir"></div>
		</noindex>
		<!-- Выбор варианта товара (The End) -->
		
		{else}
			Нет в наличии
		{/if}
			
			
			
	</div>
	<!-- Описание товара (The End)-->

	<!-- Дополнительные фото продукта -->
	{if $product->images|count>1}
	<div class="images">
		{* cut удаляет первую фотографию, если нужно начать 2-й - пишем cut:2 и тд *}
		{foreach $product->images|cut as $i=>$image}
			<a href="{$image->filename|resize:800:600:w}" class="zoom" data-rel="group"><img src="{$image->filename|resize:95:95}" alt="{$product->name|escape}" /></a>
		{/foreach}
	</div>
	{/if}
	<!-- Дополнительные фото продукта (The End)-->

	
	<pre style="display: none;">{print_r($product)}</pre>		
		

	<div style="clear: both;"></div>
	<div class="tabs">
		<ul class="tabs_menu">
			<li><a href="#tab_descr">Описание</a></li>
			<li><a href="#tab_features" class="active">Характеристики</a></li>
			<li><a href="#tab_complect">Комплектация</a></li>
			<li><a href="#tab_video">Видео</a></li>
			<li><a href="#tab_comments">Отзывы</a></li>
		</ul>
		<div class="tabs_content">
			<div class="tab" id="#tab_descr" style="display: none;">
			
			{$product->short_descr}
			
			</div>
			<div class="tab" id="#tab_features">
	
			{if $product->features}
				<!-- Характеристики товара -->
				<ul class="features">
				{foreach $product->features as $f}
					<li>
						<label>{$f->name}</label>
						<span>{$f->value}</span>
					</li>
				{/foreach}
				</ul>
				<!-- Характеристики товара (The End)-->
			{/if}
			
			</div>
			<div class="tab" id="#tab_complect" style="display: none;">
			
			{if $product->complect}
				{$product->complect}
			{/if}
			
			</div>
			<div class="tab" id="#tab_video" style="display: none;">
	
			{$product->video}
			
			</div>
			<div class="tab" id="#tab_comments" style="display: none;">
			

				{include file="forms/form_comments.html"}
				

			</div>
		</div>
	</div>	

	{literal}
	<style>
		.tabs{width: 100%; margin: 20px 0;}
		.tabs ul.tabs_menu{display: flex; justify-content: space-between; list-style: none;}
			.tabs ul.tabs_menu li{flex-grow: 2;}
			.tabs ul.tabs_menu li a{
				display: block;
				text-align: center;
				padding: 10px 10px;
				color: #125cb1;
				background: #d7d7d7;
				transition: all .3s;
				text-decoration: none;
			}
				.tabs ul.tabs_menu li a.active{
					color: #fff;
					background: #125cb1;
				}
			
			.tab{width: 100%; padding: 10px 20px; box-sizing: border-box;}
	</style>	
	<script>
		function changeTab(el) {
			var id = el.attr('href');
			$('.tabs_menu a.active').removeClass('active');
			el.addClass('active');
			
			$('.tab').hide();
			document.getElementById(id).style.display = 'block'; // $(id) - wtf, не работает
		}
	
		$(".tabs_menu a").click(function(e){
			e.preventDefault();
			changeTab($(this));
		});
	</script>
	{/literal}
	


	<!-- Соседние товары /-->
	<div id="back_forward">
		{if $prev_product}
			←&nbsp;<a class="prev_page_link" href="products/{$prev_product->url}">{$prev_product->name|escape}</a>
		{/if}
		{if $next_product}
			<a class="next_page_link" href="products/{$next_product->url}">{$next_product->name|escape}</a>&nbsp;→
		{/if}
	</div>
	
</div>
<!-- Описание товара (The End)-->

{* Рандомные связанные товары *}
{if $random_products}
<h2>Также советуем посмотреть</h2>
<!-- Список каталога товаров-->
<ul class="tiny_products">
	{foreach $random_products as $product}
	<!-- Товар-->
	<li class="product">
		<!-- Фото товара -->
		{if $product->image}
		<div class="image">
			<a href="products/{$product->url}"><img src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}"/></a>
		</div>
		{/if}
		<!-- Фото товара (The End) -->

		<!-- Название товара -->
		<h3><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h3>
		<!-- Название товара (The End) -->

		{if $product->variants|count > 0}
		<!-- Выбор варианта товара -->
		<form class="variants" action="/cart">
			<table>
			{foreach $product->variants as $v}
			<tr class="variant">
				<td>
					<input id="related_{$v->id}" name="variant" value="{$v->id}" type="radio" class="variant_radiobutton"  {if $v@first}checked{/if} {if $product->variants|count<2} style="display:none;"{/if}/>
				</td>
				<td>
					{if $v->name}<label class="variant_name" for="related_{$v->id}">{$v->name}</label>{/if}
				</td>
				<td>
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="price">Цена: {$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
				</td>
			</tr>
			{/foreach}
			</table>
			<input type="submit" class="button" value="в корзину" data-result-text="добавлено"/>
			<br/><a class="button1" onClick="modal_state('one_click','block');  setProduct('{$product->id}','{$product->name|escape}');">Хочу дешевле</a>
		</form>
		<!-- Выбор варианта товара (The End) -->
		{else}
			Нет в наличии
		{/if}
	</li>
	<!-- Товар (The End)-->
	{/foreach}
</ul>
{/if}


{literal}
<script>
$(function() {
	// Раскраска строк характеристик
	$(".features li:even").addClass('even');

	// Зум картинок
	$("a.zoom").fancybox({ 'hideOnContentClick' : true });
});
</script>
{/literal}
