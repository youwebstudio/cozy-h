{* Страница с формой обратной связи *}

<h1>{$page->name|escape}</h1>

{$page->body}



<div itemscope="" itemtype="http://schema.org/PostalAddress">
<p><span style="font-weight: bold">Адрес в г. Санкт-Петербург:</span></p>
<p>Адрес местонахождения: <span itemprop="addressLocality">г. Санкт-Петербург</span>, <span itemprop="streetAddress">2-ой верхний переулок д. 13 лит А, офис 40</span>.<br /> 
Юридический адрес: <span itemprop="addressLocality">г. Санкт-Петербург</span>, <span itemprop="streetAddress">Набережная обводного канала, д. 199</span>.<br /> 
Телефон: <span itemprop="telephone" class="roistat-phone7812">+7 (812) 409-47-79</span><br />
E-mail: <span itemprop="email"><a href="mailto:office@cozy-h.ru">office@cozy-h.ru</a></span></p>
<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=Ldv5SIUblwCKEgFCZGEOgMYsygznoYqh&amp;width=535&amp;height=369&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>


<hr />
<p></p>

<p><span style="font-weight: bold">Адрес в г. Москва:</span></p>
<p><span itemprop="addressLocality">Москва</span>, <span itemprop="streetAddress">Рязанский проспект, 4а</span>.<br /> 
Телефон: <span itemprop="telephone" class="roistat-phone7499">8 (499) 502-95-12</span><br />
E-mail: <span itemprop="email"><a href="mailto:office@cozy-h.ru">office@cozy-h.ru</a></span></p>
<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=WDfDAnuCPhjK8Q7nT0JhtFrjY8OuMg9C&amp;width=535&amp;height=369&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>

<p><br /></p>
<p><span itemprop="name">Общество с ограниченной ответственностью&nbsp; &laquo;Экотрейд&raquo;</span></p>
<p>ИНН 7736266228</p>
<p>КПП 773601001</p>
<p>ОГРН 1167746429267</p>
<p>ОКПО 02243505</p>
<p>р/с 40702810402310001059 в АО &laquo;АЛЬФА-БАНК&raquo;</p>
<p>БИК 044525593 к/с 30101810200000000593</p>
<p>Юр. Адрес 119331 г. Москва, Проспект Вернадского д 29 этаж 12 пом. 1 к. 5</p>
<p>Генеральный директор Иванников Алексей Андреевич&nbsp;</p>
</div>






<h2>Обратная связь</h2>

{if $message_sent}
<script>
jQuery(window).load(function(){
evn(1)
});</script>
	{$name|escape}, ваше сообщение отправлено.
{else}
<form class="form feedback_form" method="post">
	{if $error}
	<div class="message_error">
		{if $error=='captcha'}
		Неверно введена капча
		{elseif $error=='empty_name'}
		Введите имя
		{elseif $error=='empty_email'}
		Введите email
		{elseif $error=='empty_text'}
		Введите сообщение
		{/if}
	</div>
	{/if}
	<label>Имя</label>
	<input data-format=".+" data-notice="Введите имя" value="{$name|escape}" name="name" maxlength="255" type="text"/>
 
	<label>Email</label>
	<input data-format="email" data-notice="Введите email" value="{$email|escape}" name="email" maxlength="255" type="text"/>
	
	<label>Сообщение</label>
	<textarea data-format=".+" data-notice="Введите сообщение" value="{$message|escape}" name="message">{$message|escape}</textarea>

	<input class="button" type="submit" name="feedback" value="Отправить" />

	<div class="captcha"><img src="captcha/image.php?{math equation='rand(10,10000)'}"/></div> 
	<input class="input_captcha" id="comment_captcha" type="text" name="captcha_code" value="" data-format="\d\d\d\d" data-notice="Введите капчу"/>
	
</form>
{/if}