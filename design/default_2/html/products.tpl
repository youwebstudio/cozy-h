{if $page->id == 7 && $current_page_num == 1}
{$meta_title = "Все товары (1 страница) – купить водоочистительное оборудование в Санкт-Петербурге и Москве – Уютный дом" scope=parent}
{$meta_description = "Интернет-магазин Уютный дом предлагает безреагнетное водоочистительное оборудование для квартир и загородных домов. В каталоге представлен широкий ассортимент систем водоочистки." scope=parent}
{$meta_keywords = "все, товар, 1, страница, купить, водоочистительный, оборудование, санкт-петербург, москва, уютный, дом" scope=parent}
{/if}

{if $page->id == 7 && $current_page_num == 2}
{$meta_title = "Все товары (2 страница) – купить систему водоочистки в Санкт-Петербурге и Москве – Уютный дом" scope=parent}
{$meta_description = "На странице представлен огромный ассортимент систем водоочистки. Мы стремимся учитывать потребности каждого клиента, поэтому мы предлагаем разные технологии очистки воды: от обратного осмоса до аэрации." scope=parent}
{$meta_keywords = "все, товар, 2, страница, купить, система, водоочистка, санкт-петербург, москва, уютный, дом" scope=parent}
{/if}

{if $page->id == 7 && $current_page_num == 3}
{$meta_title = "Все товары (3 страница) – заказать водоочистительное оборудование в Санкт-Петербурге и Москве – Уютный дом" scope=parent}
{$meta_description = "Предлагаем безреагнетное водоочистительное оборудование для квартир и загородных домов. Если Вы затрудняетесь в поисках подходящего фильтра, тогда Вы можете связаться с нашими менеджерами по форме обратной связи." scope=parent}
{$meta_keywords = "все, товар, 3, страница, заказать, водоочистительный, оборудование, санкт-петербург, москва, уютный, дом" scope=parent}
{/if}

{if $page->id == 7 && $current_page_num == 4}
{$meta_title = "Все товары (4 страница) – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом" scope=parent}
{$meta_description = "Купить безреагнетное водоочистительное оборудование для квартир и загородных домов можно на сайте интернет-магазина Уютный дом. У нас в каталоге Вы найдете широкий ассортимент систем водоочистки." scope=parent}
{$meta_keywords = "все, товар, 4, страница, продажа, фильтр, санкт-петербург, москва, интернет, магазин, уютный, дом" scope=parent}
{/if}

{if $page->id == 7 && $current_page_num == 5}
{$meta_title = "Все товары (5 страница), цены на системы водоочистки в Санкт-Петербурге и Москве – Уютный дом" scope=parent}
{$meta_description = "На сайте интернет-магазина Уютный дом представлен каталог безреагнетного водоочистительного оборудования для квартир и загородных домов. У нас Вы сможете найти подходящие по типу и цене фильтры." scope=parent}
{$meta_keywords = "все, товар, 5, страница, цена, система, водоочистка, санкт-петербург, москва, уютный, дом" scope=parent}
{/if}

{if $page->id == 7 && $current_page_num == 6}
{$meta_title = "Все товары (6 страница) – купить водоочистительное оборудование в Санкт-Петербурге и Москве – Уютный дом" scope=parent}
{$meta_description = "Предлагаем широкий ассортимент недорогого водоочистительного оборудования ведущих брендов. Купить подходящий фильтр можно по ценам, указанным в каталоге интернет-магазина." scope=parent}
{$meta_keywords = "все, товар, 6, страница, купить, водоочистительный, оборудование, санкт-петербург, москва, уютный, дом" scope=parent}
{/if}

{if $page->id == 7 && $current_page_num == 7}
{$meta_title = "Все товары (7 страница) – стоимость водоочистительного оборудования в Санкт-Петербурге и Москве – Уютный дом" scope=parent}
{$meta_description = "Интернет-магазин Уютный дом осуществляет продажу безреагнетного водоочистительного оборудования. Подобрать хорошую систему водоочистки Вы можете в нашем каталоге с ценами." scope=parent}
{$meta_keywords = "все, товар, 7, страница, стоимость, водоочистительный, оборудование, санкт-петербург, москва, уютный, дом" scope=parent}
{/if}

{if $page->id == 7 && $current_page_num == 8}
{$meta_title = "Все товары (8 страница) – купить в Санкт-Петербурге и Москве водоочистительное оборудование – интернет-магазин Уютный дом" scope=parent}
{$meta_description = "Уютный дом осуществляет продажу водоочистительного оборудования ведущих производителей. Чтобы купить хорошую систему водоочистки по ценам, представленным в каталоге, добавьте выбранные товары в Корзину." scope=parent}
{$meta_keywords = "все, товар, 8, страница, купить, санкт-петербург, москва, водоочистительный, оборудование, интернет, магазин, уютный, дом" scope=parent}
{/if}

{if $page->id == 7 && $current_page_num == 9}
{$meta_title = "Все товары (9 страница) – заказать систему водоочистки в Санкт-Петербурге и Москве – Уютный дом" scope=parent}
{$meta_description = "На сайте интернет-магазина Уютный дом представлен широкий ассортимент водоочистительного оборудования ведущих брендов. Выбрать из каталога и купить лучшую систему водоочистки можно с доставкой в Москве и в регионы." scope=parent}
{$meta_keywords = "все, товар, 9, страница, заказать, система, водоочистка, санкт-петербург, москва, уютный, дом" scope=parent}
{/if}

{* Список товаров *}

<!-- Хлебные крошки /-->
<div id="path">
	<a href="/">Главная</a>
	{if $category}
	{foreach from=$category->path item=cat}
	→ <a href="catalog/{$cat->url}">{$cat->name|escape}</a>
	{/foreach}
	{if $brand}
	→ <a href="catalog/{$cat->url}/{$brand->url}">{$brand->name|escape}</a>
	{/if}
	{elseif $brand}
	→ <a href="brands/{$brand->url}">{$brand->name|escape}</a>
	{elseif $keyword}
	→ Поиск
	{/if}
</div>
<!-- Хлебные крошки #End /-->


{* Заголовок страницы *}
{if $keyword}
<h1>Поиск1 {$keyword|escape}</h1>
{elseif $page}
<h1>{$page->name|escape}</h1>
	{elseif $pageName}
    <h1>{$pageName|escape}</h1>
{else}
<h1>{$category->name|escape} {$brand->name|escape} {$keyword|escape}</h1>
{/if}

<!-- Описание вверху -->
{if $current_page_num==1}
    {$category->top_description}
{/if}
<!-- Описание вверху / End -->

{* Описание страницы (если задана) *}
{$page->body}

{* Фильтр по брендам *}
{if $category->brands}
<div id="brands">
	{if (count($category->brands) > 1)}
		<!--<a href="catalog/{$category->url}" {if !$brand->id}class="selected"{/if}>Все бренды</a>-->
	{/if}
	{foreach name=brands item=b from=$category->brands}
		{if $b->image}
		<a data-brand="{$b->id}" href="catalog/{$category->url}/{$b->url}"><img src="{$config->brands_images_dir}{$b->image}" alt="{$b->name|escape}"></a>
		{else}
		<a data-brand="{$b->id}" href="catalog/{$category->url}/{$b->url}" {if $b->id == $brand->id}class="selected"{/if}>{$b->name|escape}</a>
		{/if}
	{/foreach}
</div>
{/if}

{* Описание бренда *}
{$brand->description}


{* Фильтр по свойствам *}
{if $features}
<table id="features">
	{foreach $features as $f}
	<tr>
	<td class="feature_name" data-feature="{$f->id}">
		{$f->name}:
	</td>
	<td class="feature_values">
		<a href="{url params=[$f->id=>null, page=>null]}" {if !$smarty.get.$f@key}class="selected"{/if}>Все</a>
		{foreach $f->options as $o}
		<a href="{url params=[$f->id=>$o->value, page=>null]}" {if $smarty.get.$f@key == $o->value}class="selected"{/if}>{$o->value|escape}</a>
		{/foreach}
	</td>
	</tr>
	{/foreach}
</table>
{/if}


<!--Каталог товаров-->
{if $products}

{* Сортировка *}
{if $products|count>0}
<div class="sort">
	Сортировать по
	<!--<a {*if $sort=='position'*} class="selected"{*/if*} href="{*url sort=position page=null*}">умолчанию</a>-->
	<a {if $sort=='price'}    class="selected"{/if} href="{url sort=price page=null}">цене</a>
	<a {if $sort=='name'}     class="selected"{/if} href="{url sort=name page=null}">названию</a>
	<!--<a {*if $sort=='position'*} class="selected"{*/if*} href="{*url sort=position page=null*}">умолчанию</a>
	<a {*if $sort=='price'*}    class="selected"{*/if*} href="{*url sort=price page=null*}">цене</a>-->
</div>
{/if}


{include file='pagination.tpl'}

<pre style="display: none;">
{$brand_page}
</pre>

{if (!is_object($brand))}
	<noindex>
{/if}

<!-- Список товаров-->
<ul class="products">

	{foreach $products as $product}
	<!-- Товар-->
	<li class="product">

		<!-- Фото товара -->
		{if $product->image}
		<div class="image">
			<a href="products/{$product->url}"><img src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}"/></a>
		</div>
		{/if}
		<!-- Фото товара (The End) -->

		<div class="product_info">
		<!-- Название товара -->
		<h3 class="{if $product->featured}featured{/if}"><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h3>
		<!-- Название товара (The End) -->

		<!-- Описание товара -->
		<div class="annotation">{$product->annotation}</div>
		<!-- Описание товара (The End) -->

		{if $product->variants|count > 0}
		<!-- Выбор варианта товара -->

		<div class="left-wrap">

			<form class="variants" action="/cart">
				<table>
				{foreach $product->variants as $v}
				<tr class="variant">
					<td>
						<input id="variants_{$v->id}" name="variant" value="{$v->id}" type="radio" class="variant_radiobutton" {if $v@first}checked{/if} {if $product->variants|count<2}style="display:none;"{/if}/>
					</td>
					<td>
						{if $v->name}<label class="variant_name" for="variants_{$v->id}">{$v->name}</label>{/if}
					</td>
					<td>
						{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
						<span class="price">Цена: {$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
					</td>
				</tr>
				{/foreach}
				</table>
				<p class="available">В наличии</p>
				<input type="submit" class="button" value="в корзину" data-result-text="добавлено"/>
				<br/><a class="button1" onClick="modal_state('one_click', 'block'); setProduct('{$product->id}','{$product->name|escape}')">Хочу дешевле</a>
			</form>


		</div>
		<div class="right-wrap">

			<div class="rating">

				<p>Рейтинг: <b class="rating-value">{$product->rating}</b></p>
				<div class="rating-block">
					<div class="rating-stars" style="width: 100%;"></div>
					<div class="rating-active" style="width: {$product->rating*20}%;"></div>
				</div>
			</div>

			<div class="warranty">
				<p><b>Гарантия</b><br/>5 лет</p>
			</div>

		</div>
		<div style="clear: both;"></div>

		<!-- Выбор варианта товара (The End) -->
		{else}
			Нет в наличии
		{/if}

		</div>

	</li>
	<!-- Товар (The End)-->
	{/foreach}

</ul>

{if (!is_object($brand))}
	</noindex>
{/if}

{include file='pagination.tpl'}
<!-- Список товаров (The End)-->

{else}

{/if}
<!--Каталог товаров (The End)-->


{if (is_object($brand))}
	<noindex>
{/if}

{if $page_n==1}
	{* Описание категории *}
	{$category->description}
{/if}

{if (is_object($brand))}
	</noindex>
{/if}