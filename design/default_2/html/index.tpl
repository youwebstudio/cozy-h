<!DOCTYPE html>
{*
	Общий вид страницы
	Этот шаблон отвечает за общий вид страниц без центрального блока.
*}
<html>
<head>
	<base href="{$config->root_url}/"/>
	<title>{$meta_title|escape}</title>
	
	{* Метатеги *}
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="{$meta_description|escape}" />
	<meta name="keywords"    content="{$meta_keywords|escape}" />
	<meta name="viewport" content="width=1024"/>
	
	{* Стили *}
	<link href="design/{$settings->theme|escape}/css/style.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="design/{$settings->theme|escape}/images/favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="design/{$settings->theme|escape}/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

	{* JQuery *}
	<script src="js/jquery/jquery.js"  type="text/javascript"></script>
	
	<!-- Фильтрация -->
	<link rel="stylesheet" href="/design/default_2/css/jquery-ui.css">
	<link rel="stylesheet" href="/design/default_2/css/jquery-ui-custom.css">
	<script src="/design/default_2/js/jquery-ui.js" type="text/javascript"></script>
	<script src="/design/default_2/js/main.js" type="text/javascript"></script>
	{literal}
	<script>
		$(function() {
//			$('.totalBlock a[href="'+window.location.pathname+'"]').addClass('filtered');
//			$('.totalBlock a[href="'+window.location.pathname+'"] input[type="checkbox"]').attr('checked', 'checked');
//			$('.totalBlock a[href="'+window.location.pathname+'"]').closest('.totalBlock').addClass('active');

//			$(".item-checkbox").click(function () {
//				if ($(this).is(":checked") )
//					window.location = $(this).parent().attr("href");
//            });
		});
	</script>
	{/literal}
	
	{* Всплывающие подсказки для администратора *}
	{if $smarty.session.admin == 'admin'}
	<script src ="js/admintooltip/admintooltip.js" type="text/javascript"></script>
	<link   href="js/admintooltip/css/admintooltip.css" rel="stylesheet" type="text/css" /> 
	{/if}
	
	{* Увеличитель картинок *}
	<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" href="js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
	
	{* Ctrl-навигация на соседние товары *}
	<script type="text/javascript" src="js/ctrlnavigate.js"></script>           
	
	{* Аяксовая корзина *}
	<script src="design/{$settings->theme}/js/jquery-ui.min.js"></script>
	<script src="design/{$settings->theme}/js/ajax_cart.js"></script>
	
	{* js-проверка форм *}
	<script src="/js/baloon/js/baloon.js" type="text/javascript"></script>
	<link   href="/js/baloon/css/baloon.css" rel="stylesheet" type="text/css" /> 
	
	{* Автозаполнитель поиска *}
	{literal}
	<script src="js/autocomplete/jquery.autocomplete-min.js" type="text/javascript"></script>
	<style>
	.autocomplete-w1 { position:absolute; top:0px; left:0px; margin:6px 0 0 6px; /* IE6 fix: */ _background:none; _margin:1px 0 0 0; }
	.autocomplete { border:1px solid #999; background:#FFF; cursor:default; text-align:left; overflow-x:auto;  overflow-y: auto; margin:-6px 6px 6px -6px; /* IE6 specific: */ _height:350px;  _margin:0; _overflow-x:hidden; }
	.autocomplete .selected { background:#F0F0F0; }
	.autocomplete div { padding:2px 5px; white-space:nowrap; }
	.autocomplete strong { font-weight:normal; color:#3399FF; }
	</style>	
	<script>
	$(function() {
		//  Автозаполнитель поиска
		$(".input_search").autocomplete({
			serviceUrl:'ajax/search_products.php',
			minChars:1,
			noCache: false, 
			onSelect:
				function(value, data){
					 $(".input_search").closest('form').submit();
				},
			fnFormatResult:
				function(value, data, currentValue){
					var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
					var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
	  				return (data.image?"<img align=absmiddle src='"+data.image+"'> ":'') + value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
				}	
		});
	});
	</script>
        
        
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter34948605 = new Ya.Metrika({
                    id:34948605,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/34948605" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
		
                
<script src="/GA.js" type="text/javascript"></script>
	{/literal}

	{*SET filter_data*}
	{if $filter_data}
		<script type="text/javascript">
			window.filter_data = {$filter_data};
		</script>
	{/if}
</head>
<body>
	<!-- фильтрация по цене -->
	{get_data var=result class=filter method=get_prices}
	<input name="filter-prices" type="hidden" value="{$result->prices}">

	<!-- Верхняя строка -->
	<div id="top_background">
	<div id="top">
	
		<!-- Меню -->
		<ul id="menu">
			{foreach $pages as $p}
				{* Выводим только страницы из первого меню *}
				{if $p->menu_id == 1}
				<li {if $page && $page->id == $p->id}class="selected"{/if}>
					<a data-page="{$p->id}" href="{$p->url}">{$p->name|escape}</a>
				</li>
				{/if}
			{/foreach}
		</ul>
		<!-- Меню (The End) -->
	
		<!-- Корзина -->
		<div id="cart_informer">
			{* Обновляемая аяксом корзина должна быть в отдельном файле *}
			{include file='cart_informer.tpl'}
		</div>
		<!-- Корзина (The End)-->

		<!-- Вход пользователя -->
		<div id="account">
			{if $user}
				<span id="username">
					<a href="user">{$user->name}</a>{if $group->discount>0},
					ваша скидка &mdash; {$group->discount}%{/if}
				</span>
				<a id="logout" href="user/logout">выйти</a>
			{else}
				<a id="register" href="user/register">Регистрация</a>
				<a id="login" href="user/login">Вход</a>
			{/if}
		</div>
		<!-- Вход пользователя (The End)-->

	</div>
	</div>
	<!-- Верхняя строка (The End)-->
	
	
	<!-- Шапка -->
	<div id="header">
		<div id="logo">
			<a href="/"><img src="design/{$settings->theme|escape}/images/logo940.png" title="{$settings->site_name|escape}" alt="{$settings->site_name|escape}"/></a>
		</div>	
                <div style="float: left; text-align: center; margin-top: 15px; margin-left: 90px;">
                 <p style="font-size: 21px; margin-bottom: 0px;">Есть вопросы?<br>Мы вам перезвоним!</p>
                <br>
                <a onClick="show('block')" class="button" style="text-align: center;">Заказать звонок</a>
                </div>
		<div id="contact">
			<span id="phone" class="roistat-phone7812">+7 (812) 409-47-79</span>
                 <div id="address"><a href="mailto:office@cozy-h.ru">office@cozy-h.ru</a></div>
                        <div id="address">Санкт-Петербург,</div>
                        <div id="address">Набережная обводного канала</div>
                        <div id="address">д. 199</div>
		</div>	
	</div>
	<!-- Шапка (The End)--> 


	<!-- Вся страница --> 
	<div id="main">

		<!-- Основная часть --> 
		<div id="content">
			{$content}
		</div>
		<!-- Основная часть (The End) --> 
	
		<div id="left">

			<!-- Поиск-->
			<div id="search">
				<form action="products">
					<input class="input_search" type="text" name="keyword" value="{$keyword|escape}" placeholder="Поиск товара"/>
					<input class="button_search" value="" type="submit" />
				</form>
			</div>
			<!-- Поиск (The End)-->

			
			<!-- Меню каталога -->
			<div id="catalog_menu">

                {get_brands var=all_brands}
                <div class="category-caption by-params">Фильтр</div>
					{if $all_brands}
						<!-- <div class="category-caption"></div> -->
						<div class="totalBlock active">
							<div class="nameBlock">Бренды</div>
							<div class="m-filter-wrapper">
								<div class="m-filter-expand-collapse"></div>
							</div>
							<ul class="category-list elementsBlock">
								{foreach $all_brands as $b}
									{get_data var=result class=filter method=get_brand_items_count brand=$b->id}
									<li>
										{assign var=curUri value="/brands/"|cat:$b->url}
										<a data-type="brand" data-value="{$b->id}" class="js-filter js-filter-brand {if $smarty.server.REQUEST_URI eq $curUri}active{/if}" href="/brands/{$b->url}">
											<span class="point"></span>{$b->name} <span class="count">({$result->count})</span>
										</a>
									</li>
								{/foreach}
							</ul>
						</div>
						<!-- <div class="category-list">
						</div> -->
					{/if}
                	<div class="totalBlock active block-slider">
						<div class="nameBlock">Цена</div>
						<div class="m-filter-wrapper">
							<div class="m-filter-expand-collapse"></div>
						</div>
						<div class="elementsBlock">
							<div class="elementBlock">
								<div id="price-range" class="js-filter"></div>
								<div id="price-amount"></div>
							</div>
							<div id="prices-block">
								<input name="start" type="text" value="">
								<div class="bold">до</div>
								<input name="finish" type="text" value="">
							</div>
						</div>
					</div>
					{get_categories var=all_categories}
					{if $all_categories}
						{foreach $all_categories as $category}
							<div class="totalBlock active">
								<div class="nameBlock">{$category->name}</div>
								<div class="m-filter-wrapper">
									<div class="m-filter-expand-collapse"></div>
								</div>
								<ul class="category-list elementsBlock">
                                    {foreach $category->subcategories as $sub_category}
										<li>
                                            {get_data var=result class=filter method=get_count_products_by_parent parent=$sub_category->name}
                                            {if $result}
												<a data-type="category" data-value="{$sub_category->id}" class="js-filter js-filter-catalog" href="/catalog/{$result->url}">
													<span class="point"></span>{$sub_category->name} <span class="count">({$result->count})</span>
												</a>
                                            {/if}
										</li>
                                    {/foreach}
								</ul>
							</div>
						{/foreach}
					{/if}
				<!--
					{*<div class="totalBlock active">*}
						{*<div class="nameBlock">Вид</div>*}
						{*<div class="m-filter-wrapper">*}
							{*<div class="m-filter-expand-collapse"></div>*}
						{*</div>*}
						{*<ul class="elementsBlock">*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Магистральные фильтры"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}">*}
										{*<input type="checkbox" class="item-checkbox">*}
										{*Магистральные фильтры <span class="count">({$result->count})</span>*}
									{*</a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Сорбционные фильтры"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Сорбционные фильтры <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Системы обратного осмоса"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Системы обратного осмоса <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Фильтры для умягчения воды"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Фильтры для умягчения воды <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Фильтры тонкой механической очистки"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Фильтры тонкой механической очистки <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Фильтры грубой механической очистки"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Фильтры грубой механической очистки <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Комплексные системы водоочистки"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Комплексные системы водоочистки <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
						{*</ul>*}
						{*<ul class="elementsBlock">*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Механические фильтры"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}">*}
										{*<input type="checkbox" class="item-checkbox">*}
										{*Механические <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Угольные фильтры"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Угольные <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Ультрафиолетовые стерилизаторы"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Ультрафиолетовые стерилизаторы <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Универсальные кабинетные фильтры"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Универсальные <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
						{*</ul>*}
					{*</div>*}
					{*<div class="totalBlock active">*}
						{*<div class="nameBlock">Характер проживания</div>*}
						{*<div class="m-filter-wrapper">*}
							{*<div class="m-filter-expand-collapse"></div>*}
						{*</div>*}
						{*<ul class="elementsBlock">*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Фильтры для дома"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Фильтры для дома (постоянное проживание) <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Фильтры для коттеджей"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Фильтры для коттеджей (постоянное проживание) <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Фильтры для очистки воды на даче"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Фильтры для очистки воды на даче (сезонное проживание) <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
						{*</ul>*}
					{*</div>*}
					{*<div class="totalBlock active">*}
						{*<div class="nameBlock">Назначение</div>*}
						{*<div class="m-filter-wrapper">*}
							{*<div class="m-filter-expand-collapse"></div>*}
						{*</div>*}
						{*<ul class="elementsBlock">*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Фильтры для обезжелезивания воды из скважины"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Фильтры для обезжелезивания воды из скважины <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Фильтры очистки воды из скважины"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Фильтры очистки воды из скважины <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Фильтры очистки воды от железа"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Фильтры очистки воды от железа <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Обеззараживание воды"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Обеззараживание воды <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
							{*<li>*}
								{*{get_data var=result class=filter method=get_count_products_by_parent parent="Очистка воды аэрацией. Системы аэрации"}*}
								{*{if $result}*}
									{*<a class="js-filter js-filter-catalog" href="/catalog/{$result->url}"><input type="checkbox" class="item-checkbox">Очистка воды аэрацией <span class="count">({$result->count})</span></a>*}
								{*{/if}*}
							{*</li>*}
						{*</ul>*}
					{*</div>*}
				-->
				{get_data var=result class=filter method=get_count_products_global}
				{foreach $result as $category}
				
					{if $category->name == "Максимальная температура воды"}
						<div class="category-caption">Технические характеристики</div>
					{/if}

					{if $category->name == "Использование"}
						<div class="totalBlock active">
					{else}
						<div class="totalBlock">
					{/if}
	
				
						<div class="nameBlock">{$category->name} </div>
						<div class="m-filter-wrapper">
							<div class="m-filter-expand-collapse"></div>
						</div>
						<ul class="category-list elementsBlock">
							{foreach $category->items as $item}
								<li>
									{*{$item|@print_r}*}
									<a data-type="feature" data-id="{$item->feature_id}" data-value="{$item->value}" class="js-filter js-filter-catalog" href="/catalog/{$item->url}">
										<span class="point"></span>{$item->value} <span class="count">({$item->count})</span>
									</a>
								</li>
							{/foreach}
						</ul>
					</div>
                {/foreach}
			</div>
			<!-- Меню каталога (The End)-->		

			<!-- Выбор валюты -->
			{* Выбор валюты только если их больше одной *}
			{if $currencies|count>1}
			<div id="currencies">
				<h2>Валюта</h2>
				<ul>
					{foreach from=$currencies item=c}
					{if $c->enabled} 
					<li class="{if $c->id==$currency->id}selected{/if}"><a href='{url currency_id=$c->id}'>{$c->name|escape}</a></li>
					{/if}
					{/foreach}
				</ul>
			</div> 
			{/if}
			<!-- Выбор валюты (The End) -->	

			
			<!-- Просмотренные товары -->
			{get_browsed_products var=browsed_products limit=20}
			{if $browsed_products}
			
				<h2>Вы просматривали:</h2>
				<ul id="browsed_products">
				{foreach $browsed_products as $browsed_product}
					<li>
					<a href="products/{$browsed_product->url}"><img src="{$browsed_product->image->filename|resize:50:50}" alt="{$browsed_product->name}" title="{$browsed_product->name}"></a>
					</li>
				{/foreach}
				</ul>
			{/if}
			<!-- Просмотренные товары (The End)-->
			
			
			<!-- Меню блога -->
			{* Выбираем в переменную $last_posts последние записи *}
			{get_posts var=last_posts limit=5}
			{if $last_posts}
			<div id="blog_menu">
				<h2>Наши свежие <a href="blog">новости</a></h2>
				{foreach $last_posts as $post}
				<ul>
					<li data-post="{$post->id}">{$post->date|date} <a href="blog/{$post->url}">{$post->name|escape}</a></li>
				</ul>
				{/foreach}
			</div>
			{/if}
			<!-- Меню блога  (The End) -->
			
		</div>			
		
	</div>
	<!-- Вся страница (The End)--> 
	
	<!-- Футер -->
	<div id="footer">
		<table>
		    <tbody>
			    <tr>
				    <td class="block_1">
					    <p>О компании</p>
					    <ul>
						    <li><a href="/blog">Новости</a></li>
							<li><a href="/dostavka">Доставка</a></li>
							<li><a href="/oplata">Оплата</a></li>
							<li><a href="/contact">Контакты</a></li>
							<li><a href="/sitemap">Карта сайта</a></li>
						</ul>
					</td>
					
					<td class="block_2">
					    <p>Системы очистки воды</p>
					    <ul>
						    <li><a href="/catalog/filtry-dlya-doma">Фильтры для дома</a></li>
							<li><a href="/catalog/filtry-dlya-kottedjey">Фильтры для коттеджей</a></li>
							<li><a href="/catalog/ochistka-vody-iz-skvazhiny">Фильтры очистки воды из скважины</a></li>
							<li><a href="/catalog/filtry-dlya-dachi">Фильтры очистки воды для дачи</a></li>
							<li><a href="/catalog/filtry-obezzhelezivaniya">Фильтры для обезжелезивания воды из скважины</a></li>
						</ul>
					</td>
					
					<td class="block_3">
					    <p>Оборудование</p>
					    <ul>
						    <li><a href="/catalog/kompleksnye-sistemy-vodoochistki">Комплексные системы водоочистки</a></li>
							<li><a href="/catalog/sistemy-obratnogo-osmosa">Системы обратного осмоса</a></li>
							<li><a href="/catalog/magistralnye-filtry">Магистральные фильтры</a></li>
							<li><a href="/catalog/sorbtsionnye-filtry">Сорбционные фильтры</a></li>
							<li><a href="/catalog/filtry-dlya-umyagcheniya">Фильтры для умягчения воды</a></li>
						</ul>
					</td>
		        </tr>
		    <tbody>
		</table>
		
		<p>ООО «Экотрейд», телефон: <span class="roistat-phone7812">(812) 409-47-79</span>; e-mail: office@cozy-h.ru<br/>
		г. Санкт-Петербург, Набережная обводного канала, д. 199. ИНН 7736266228 ОГРН ОГРН 1167746429267<br/>
		Сайт работает на <noindex><a rel="nofollow" href="http://halffish.ru/">halffish CMS</a></noindex></p>
		
		<p class="left">© 2016. Интернет-магазин «Уютный дом» - <a href="/">системы очистки воды</a> в Санкт-Петербурге<br/>Все права защищены.</p>
	</div>
	<!-- Футер (The End)--> 

	{include file="forms/form_feedback.html"}
	{include file="forms/form_1click.html"}
		
	{literal}  
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-82658359-1', 'auto');
		ga('send', 'pageview');

	</script>
			
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	var yaParams = {/*Здесь параметры визита*/};
	</script>

	<script type="text/javascript">
	(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
					try {
							w.yaCounter30215039 = new Ya.Metrika({id:30215039,
											webvisor:true,
											clickmap:true,
											trackLinks:true,
											accurateTrackBounce:true,params:window.yaParams||{ }});
					} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="//mc.yandex.ru/watch/30215039" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	{/literal}

	<!-- виджет обратный звонок -->
	<link rel="stylesheet" href="https://saas-support.com/widget/cbk.css">
	<script type="text/javascript" src="https://saas-support.com/widget/cbk.js?wcb_code=a23c4599c3290146074c71595b98d8e4" charset="UTF-8" async></script>
	<script>
		(function(w, d, s, h, id) {
			w.roistatProjectId = id; w.roistatHost = h;
			var p = d.location.protocol == "https:" ? "https://" : "http://";
			var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
			var js = d.createElement(s); js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
		})(window, document, 'script', 'cloud.roistat.com', 'd1c3871b645a966a8fac60cee8ae6065');
	</script>
</body>
</html>