<?php
require_once('Simpla.php');

/* файл тесно связан с /view/ProductsView.php */

class Filter extends Simpla
{
    // пустой массив указывает на то, что анкоры совпадают с параметрами выборки
    private $anchors = array(
        "water" => array(),
        "temperature" => array(
            // °
            "+45 С",
            "1-40 С",
            "100 С",
            "2-40 С",
            "2-50 С",
            "30 C",
            "300 С",
            "4-40 С",
            "4-45 С",
            "4-95 С",
            "40 С",
            "45 С",
            "5-25 С",
            "50 С",
            "70 C",
            "80 C",
            "95 С",
            "до 45 С",
            "до 70 C"
        ),
        "pressure" => array(),
        "material" => array(
            "Нержавеющая сталь",
            "Нержавеющая сталь, пластик, стекловолокно",
            "Пищевой пластик",
            "Пластик, стекловолокно",
            "Стекловолокно",
            "Стекловолокно, пищевой пластик",
            "Технический пластик"
        ),
        "disinfection" => array(),
        "ecoferox" => array(),
        "ecotar" => array(),
        "volume" => array(),
        "volume_water" => array(),
        "volume_column" => array(),
        "separate" => array(),
        "pump" => array(),
        "power" => array(),
        "size" => array(),
        "size_connection" => array(),
        "salt" => array(),
        "reset" => array(),
        "speed" => array(),
        "clean_type" => array(),
        "clean_steps" => array(),
        "height" => array(),
        "length" => array(),
        "width" => array(),
        "diameter" => array(),
        "dimensions" => array(),
    );
    private $caption = array(
        "water" => "Использование",
        "temperature" => "Максимальная температура воды",
        "pressure" => "Максимальное давление воды",
        "material" => "Материал корпуса",
        /*"disinfection" => "Обеззараживание",
        "ecoferox" => "Загрузка Ecoferox",
        "ecotar" => "Загрузка Ecotar",
        "volume" => "Накопительная емкость, объем",
        "volume_water" => "Объем воды на регенерацию",
        "volume_column" => "Объем колонны",
        "separate" => "Отдельный кран",
        "pump" => "Помпа для повышения давления",
        "power" => "Потребляемая мощность",
        "size" => "Размер картриджа",*/
        "size_connection" => "Размер присоединения",
        /*"salt" => "Расход соли на регенерацию",
        "reset" => "Сброс при промывке",*/
        "speed" => "Скорость фильтрации",
        "clean_type" => "Способ очистки",
       /* "clean_steps" => "Число ступеней очистки",*/
        "height" => "Высота",
        "length" => "Длина",
        "width" => "Ширина",
        /*"diameter" => "Диаметр",
        "dimensions" => "Габариты (ШхВхГ)",*/
    );
    private $urls = array(
        "water" => array(
            "filtry-dlja-holodnoj-vody",
            "filtry-dlja-gorjachej-vody",
            "filtry-dlja-holodnoj-i-gorjachej-vody"
        ),
        "temperature" => array(
            "filtry-s-maksimalnoj-temperaturoj-vody+45C",
            "filtry-s-maksimalnoj-temperaturoj-vody-1-40C",
            "filtry-s-maksimalnoj-temperaturoj-vody-100C",
            "filtry-s-maksimalnoj-temperaturoj-vody-2-40C",
            "filtry-s-maksimalnoj-temperaturoj-vody-2-50C",
            "filtry-s-maksimalnoj-temperaturoj-vody-30C",
            "filtry-s-maksimalnoj-temperaturoj-vody-300C",
            "filtry-s-maksimalnoj-temperaturoj-vody-4-40C",
            "filtry-s-maksimalnoj-temperaturoj-vody-4-45C",
            "filtry-s-maksimalnoj-temperaturoj-vody-4-95C",
            "filtry-s-maksimalnoj-temperaturoj-vody-40C",
            "filtry-s-maksimalnoj-temperaturoj-vody-45C",
            "filtry-s-maksimalnoj-temperaturoj-vody-5-25C",
            "filtry-s-maksimalnoj-temperaturoj-vody-50C",
            "filtry-s-maksimalnoj-temperaturoj-vody-70C",
            "filtry-s-maksimalnoj-temperaturoj-vody-80C",
            "filtry-s-maksimalnoj-temperaturoj-vody-95C",
            "filtry-s-maksimalnoj-temperaturoj-vody-do-45C",
            "filtry-s-maksimalnoj-temperaturoj-vody-do-70C"
        ),
        "pressure" => array(
            "filtry-s-maksimalnym-davleniem-vody-1-7atm",
            "filtry-s-maksimalnym-davleniem-vody-4atm",
            "filtry-s-maksimalnym-davleniem-vody-6atm",
            "filtry-s-maksimalnym-davleniem-vody-7atm",
            "filtry-s-maksimalnym-davleniem-vody-8atm",
            "filtry-s-maksimalnym-davleniem-vody-10atm",
            "filtry-s-maksimalnym-davleniem-vody-16atm",
            "filtry-s-maksimalnym-davleniem-vody-do-6atm",
            "filtry-s-maksimalnym-davleniem-vody-do-8atm",
            "filtry-s-maksimalnym-davleniem-vody-do-25atm",
            "filtry-s-maksimalnym-davleniem-vody-15bar",
            "filtry-s-maksimalnym-davleniem-vody-do-25bar",
            "filtry-s-maksimalnym-davleniem-vody-1,6MPa",
            "filtry-s-maksimalnym-davleniem-vody-2,5Mpa",
            "filtry-s-maksimalnym-davleniem-vody-do-2,5Mpa",
            "filtry-s-maksimalnym-davleniem-vody-PN16",
            "filtry-s-maksimalnym-davleniem-vody-PN25"
        ),
        "material" => array(
            "filtry-s-materialom-korpusa-nerzhavejushhaja-stal",
            "filtry-s-materialom-korpusa-nerzhavejushhaja-stal-plastik-steklovolokno",
            "filtry-s-materialom-korpusa-pishhevoj-plastik",
            "filtry-s-materialom-korpusa-plastik-steklovolokno",
            "filtry-s-materialom-korpusa-steklovolokno",
            "filtry-s-materialom-korpusa-steklovolokno-pishhevoj-plastik",
            "filtry-s-materialom-korpusa-tehnicheskij-plastik"
        ),
        "disinfection" => array(
            "filtry-s-obezzarazhivaniem",
            "filtry-bez-obezzarazhivaniya"
        ),
        "ecoferox" => array(
            "filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-1-meshok",
            "filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-2-meshka",
            "filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-3-meshka",
            "filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-4-meshka",
            "filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-5-meshkov"
        ),
        "ecotar" => array(
            "filtry-s-zagruzkoy-ecotar-1-meshok",
            "filtry-s-zagruzkoy-ecotar-1.5-meshka",
            "filtry-s-zagruzkoy-ecotar-2-meshka"
        ),
        "volume" => array(
            "filtry-s-nakopitelnoy-emkostu-obyemom-5l",
            "filtry-s-nakopitelnoy-emkostu-obyemom-12l",
            "filtry-s-nakopitelnoy-emkostu-obyemom-40l",
            "filtry-bez-nakopitelnoy-emkosti"
        ),
        "volume_water" => array(
            "filtry-s-obyemom-vody-na-regeneraciju-36l",
            "filtry-s-obyemom-vody-na-regeneraciju-60l",
            "filtry-s-obyemom-vody-na-regeneraciju-80-120l",
            "filtry-s-obyemom-vody-na-regeneraciju-100l",
            "filtry-s-obyemom-vody-na-regeneraciju-150l",
            "filtry-s-obyemom-vody-na-regeneraciju-220l",
            "filtry-s-obyemom-vody-na-regeneraciju-300l",
        ),
        "volume_column" => array(
            "filtry-s-obyemom-kolonny-11l",
            "filtry-s-obyemom-kolonny-21l",
            "filtry-s-obyemom-kolonny-33l",
            "filtry-s-obyemom-kolonny-33.6l",
            "filtry-s-obyemom-kolonny-60l",
            "filtry-s-obyemom-kolonny-61l",
            "filtry-s-obyemom-kolonny-103l",
            "filtry-s-obyemom-kolonny-105l",
            "filtry-s-obyemom-kolonny-140l",
            "filtry-s-obyemom-kolonny-170l"
        ),
        "separate" => array(
            "filtry-s-otdelnym-kranom-obratniy-osmos",
            "filtry-s-otdelnym-kranom"
        ),
        "pump" => array(
            "filtry-s-pompoy-dlja-povyshenija-davlenija",
            "filtry-bez-pompy-dlja-povyshenija-davlenija"
        ),
        "power" => array(
            "filtry-s-potrebljaemoj-moshhnostju-0.8kvt",
            "filtry-s-potrebljaemoj-moshhnostju-120w",
            "filtry-s-potrebljaemoj-moshhnostju-190w",
            "filtry-s-potrebljaemoj-moshhnostju-220w",
            "filtry-s-potrebljaemoj-moshhnostju-220v"
        ),
        "size" => array(
            "filtry-razmer-kartridzha-10-big-blue",
            "filtry-razmer-kartridzha-10-slim-line",
            "filtry-razmer-kartridzha-20-big-blue"
        ),
        "size_connection" => array(
            "filtry-s-parametrom-prisoedinenija-1-1/2",
            "filtry-s-parametrom-prisoedinenija-1-1/4",
            "filtry-s-parametrom-prisoedinenija-1",
            "filtry-s-parametrom-prisoedinenija-1/2",
            "filtry-s-parametrom-prisoedinenija-1/4",
            "filtry-s-parametrom-prisoedinenija-3/4"
        ),
        "salt" => array(
            "filtry-s-rashodom-soli-na-regeneraciju-0.5kg",
            "filtry-s-rashodom-soli-na-regeneraciju-1kg",
            "filtry-s-rashodom-soli-na-regeneraciju-1.9kg",
            "filtry-s-rashodom-soli-na-regeneraciju-2.4kg",
            "filtry-s-rashodom-soli-na-regeneraciju-3.5kg",
            "filtry-s-rashodom-soli-na-regeneraciju-5kg",
            "filtry-s-rashodom-soli-na-regeneraciju-8.5kg"
        ),
        "reset" => array(
            "filtry-sbros-pri-promyvke-12l",
            "filtry-sbros-pri-promyvke-20l",
            "filtry-sbros-pri-promyvke-60l",
            "filtry-sbros-pri-promyvke-100l",
            "filtry-sbros-pri-promyvke-150l",
            "filtry-sbros-pri-promyvke-220l",
            "filtry-sbros-pri-promyvke-250l",
            "filtry-sbros-pri-promyvke-300l",
            "filtry-sbros-pri-promyvke-320l",
            "filtry-sbros-pri-promyvke-350l",
            "filtry-sbros-pri-promyvke-400l",
            "filtry-sbros-pri-promyvke-450l",
            "filtry-sbros-pri-promyvke-ot-320l"
        ),
        "speed" => array(
            "filtry-skorost-filtracii-0.14lmin",
            "filtry-skorost-filtracii-0.25mh",
            "filtry-skorost-filtracii-0.26lmin",
            "filtry-skorost-filtracii-0.5m3h",
            "filtry-skorost-filtracii-0.6m3h",
            "filtry-skorost-filtracii-0.8m3h",
            "filtry-skorost-filtracii-0.9m3h",
            "filtry-skorost-filtracii-1m3h",
            "filtry-skorost-filtracii-1.5m3h",
            "filtry-skorost-filtracii-1.2m3h",
            "filtry-skorost-filtracii-1.8m3h",
            "filtry-skorost-filtracii-10m3h",
            "filtry-skorost-filtracii-1000lh",
            "filtry-skorost-filtracii-1140ld",
            "filtry-skorost-filtracii-1200lh",
            "filtry-skorost-filtracii-14m3h",
            "filtry-skorost-filtracii-16m3h",
            "filtry-skorost-filtracii-17.4llh",
            "filtry-skorost-filtracii-1800ld",
            "filtry-skorost-filtracii-1800lh",
            "filtry-skorost-filtracii-2.5m3h",
            "filtry-skorost-filtracii-20lmin",
            "filtry-skorost-filtracii-200lh",
            "filtry-skorost-filtracii-250ld",
            "filtry-skorost-filtracii-2500lh",
            "filtry-skorost-filtracii-3m3h",
            "filtry-skorost-filtracii-3.2m3h",
            "filtry-skorost-filtracii-3000lh",
            "filtry-skorost-filtracii-4m3h",
            "filtry-skorost-filtracii-5m3h",
            "filtry-skorost-filtracii-5.5m3h",
            "filtry-skorost-filtracii-500lh",
            "filtry-skorost-filtracii-53lm",
            "filtry-skorost-filtracii-6m3h",
            "filtry-skorost-filtracii-6000lh",
            "filtry-skorost-filtracii-7m3h",
            "filtry-skorost-filtracii-7.2m3h",
            "filtry-skorost-filtracii-7.3m3h",
            "filtry-skorost-filtracii-7.6m3h",
            "filtry-skorost-filtracii-73.3lmin",
            "filtry-skorost-filtracii-8m3h",
            "filtry-skorost-filtracii-800lh",
            "filtry-skorost-filtracii-do-1100ld",
            "filtry-skorost-filtracii-do-2lmin",
            "filtry-skorost-filtracii-do-6,5m3h"
        ),
        "clean_type" => array(
            "filtry-so-sposobom-ochistki-obratnyj-osmos",
            "filtry-so-sposobom-ochistki-obratnyj-osmos-sorbcija-umjagchenie",
            "filtry-so-sposobom-ochistki-sorbcija",
            "filtry-so-sposobom-ochistki-ultrafioletovoe-izluchenie"
        ),
        "clean_steps" => array(
            "filtry-s-chislom-stupenej-ochistki-3",
            "filtry-s-chislom-stupenej-ochistki-4",
            "filtry-s-chislom-stupenej-ochistki-5"
        ),
        "height" => array(
            "filtry-dlya-vody-vysotoy-40cm",
            "filtry-dlya-vody-vysotoy-44cm",
            "filtry-dlya-vody-vysotoy-61cm",
            "filtry-dlya-vody-vysotoy-94cm",
            "filtry-dlya-vody-vysotoy-110cm",
            "filtry-dlya-vody-vysotoy-125cm",
            "filtry-dlya-vody-vysotoy-138cm",
            "filtry-dlya-vody-vysotoy-140cm",
            "filtry-dlya-vody-vysotoy-150cm",
            "filtry-dlya-vody-vysotoy-160cm",
            "filtry-dlya-vody-vysotoy-178cm",
            "filtry-dlya-vody-vysotoy-179cm",
            "filtry-dlya-vody-vysotoy-180mm",
            "filtry-dlya-vody-vysotoy-310mm",
            "filtry-dlya-vody-vysotoy-570mm"
        ),
        "length" => array(
            "filtry-dlya-vody-dlinoy-26cm",
            "filtry-dlya-vody-dlinoy-30cm",
            "filtry-dlya-vody-dlinoy-40cm"
			
        ),
        "width" => array(
            "filtry-dlya-vody-shirinoy-55cm",
            "filtry-dlya-vody-shirinoy-60cm",
            "filtry-dlya-vody-shirinoy-65cm",
            "filtry-dlya-vody-shirinoy-80cm",
            "filtry-dlya-vody-shirinoy-110cm",
            "filtry-dlya-vody-shirinoy-120cm",
            "filtry-dlya-vody-shirinoy-150cm"
        ),
        "diameter" => array(
            "filtry-dlya-vody-diametrom-21cm",
            "filtry-dlya-vody-diametrom-26cm",
            "filtry-dlya-vody-diametrom-33cm",
            "filtry-dlya-vody-diametrom-37cm",
            "filtry-dlya-vody-diametrom-41cm",
            "filtry-dlya-vody-diametrom-130mm",
            "filtry-dlya-vody-diametrom-140mm",
            "filtry-dlya-vody-diametrom-170mm"
        ),
        "dimensions" => array(
            "filtry-dlya-vody-gabarity-140x158mm",
            "filtry-dlya-vody-gabarity-158x180mm",
            "filtry-dlya-vody-gabarity-170x449mm",
            "filtry-dlya-vody-gabarity-179x180mm",
            "filtry-dlya-vody-gabarity-20,8x59,5cm",
            "filtry-dlya-vody-gabarity-20,8x76,2cm",
            "filtry-dlya-vody-gabarity-24x8x8cm",
            "filtry-dlya-vody-gabarity-28x8x8cm",
            "filtry-dlya-vody-gabarity-29.2x55x47cm",
            "filtry-dlya-vody-gabarity-29x55x47cm",
            "filtry-dlya-vody-gabarity-38x77,5x66cm",
            "filtry-dlya-vody-gabarity-56x8x8cm",
            "filtry-dlya-vody-gabarity-57x125cm",
            "filtry-dlya-vody-gabarity-57x129cm",
            "filtry-dlya-vody-gabarity-62x139cm",
            "filtry-dlya-vody-gabarity-62x150cm",
            "filtry-dlya-vody-gabarity-62x154cm",
            "filtry-dlya-vody-gabarity-69x154cm",
            "filtry-dlya-vody-gabarity-70x45x16",
            "filtry-dlya-vody-gabarity-70x70x16",
            "filtry-dlya-vody-gabarity-78x29x70cm",
            "filtry-dlya-vody-gabarity-78x45x29",
            "filtry-dlya-vody-gabarity-81x153cm",
            "filtry-dlya-vody-gabarity-86x180cm",
            "filtry-dlya-vody-gabarity-90x17x9cm",
            "filtry-dlya-vody-gabarity-93x171cm"
        ),
    );
    private $filter = array(
        "water" => array(
            "для холодной воды",
            "для горячей воды",
            "для холодной и горячей воды"
        ),
        "temperature" => array(
            "+45 C",
            "1 - 40 C",
            "100 C",
            "2 - 40 C",
            "2 - 50 C",
            "30 C",
            "300 C",
            "4 - 40 C",
            "4 - 45 C",
            "4 - 95 C",
            "40 C",
            "45 C",
            "5 - 25 C",
            "50 C",
            "70 C",
            "80 C",
            "95 C",
            "до 45 C",
            "до 70 C"
        ),
        "pressure" => array(
            "1 - 7 атм.",
            "4 атм.",
            "6 атм.",
            "7 атм.",
            "8 атм.",
            "10 атм.",
            "16 атм.",
            "до 6 атм.",
            "до 8 атм.",
            "до 25 атм.",
            "15 бар",
            "до 25 бар",
            "1,6 МПа",
            "2,5 МПа",
            "до 2,5 МПа",
            "PN16",
            "PN25"
        ),
        "material" => array(
            "нержавеющая сталь",
            "нержавеющая сталь, пластик, стекловолокно",
            "Пищевой пластик",
            "пластик, стекловолокно",
            "стекловолокно",
            "стекловолокно, пищевой пластик",
            "технический пластик"
        ),
        "disinfection" => array(
            "да",
            "нет"
        ),
        "ecoferox" => array(
            "1 мешок",
            "2 мешка",
            "3 мешка",
            "4 мешка",
            "5 мешков"
        ),
        "ecotar" => array(
            "1 мешок",
            "1.5 мешка",
            "2 мешка"
        ),
        "volume" => array(
            "5 л",
            "12 л",
            "40 л",
            "нет"
        ),
        "volume_water" => array(
            "36 л",
            "60 л",
            "80 - 120 л",
            "100 л",
            "150 л",
            "220 л",
            "300 л"
        ),
        "volume_column" => array(
            "11 л",
            "21",
            "33 л",
            "33.6 л",
            "60 л",
            "61 л",
            "103 л",
            "105 л",
            "140 л",
            "170 л"
        ),
        "separate" => array(
            "да (обратный осмос)",
            "есть"
        ),
        "pump" => array(
            "есть",
            "нет"
        ),
        "power" => array(
            "0.8 кВт",
            "120 Вт",
            "190 Вт",
            "200 Вт",
            "220 В"
        ),

        "size" => array(
            "10 Big Blue",
            "10 Slim Line",
            "20 Big Blue"
        ),
        "size_connection" => array(
            '1 1/2"',
            '1 1/4"',
            '1"',
            '1/2"',
            '1/4"',
            '3/4"'
        ),
        "salt" => array(
            "0.5 кг",
            "1 кг",
            "1.9 кг",
            "2.4 кг",
            "3.5 кг",
            "5 кг",
            "8.5 кг"
        ),
        "reset" => array(
            "12 л.",
            "20 л.",
            "60 л.",
            "100 л.",
            "150 л.",
            "220 л.",
            "250 л.",
            "300 л.",
            "320 л.",
            "350 л.",
            "400 л.",
            "450 л.",
            "от 320 л."
        ),
        "speed" => array(
            "0.14 л/мин",
            "0.25 м3/час",
            "0.26 л/мин",
            "0.5 м3/час",
            "0.6 м3/час",
            "0.8 м3/час",
            "0.9 м3/час",
            "1 м3/час",
            "1.5 м3/час",  // ,
            "1.2 м3/час", // ,
            "1.8 м3/час", // ,
            "10 м3/ч",
            "1000 л/час",
            "1140 л/сут.",
            "1200 л/час",
            "14 м3/час",
            "16 м3/час",
            "17,4 л/час",
            "1800 л/сут",
            "1800 л/час",
            "2,5 м3/час",
            "20 л/мин",
            "200 л/час",
            "250 л/сутки",
            "2500 л/час",
            "3 м3/ч",
            "3,2 м3/час",
            "3000 л/час",
            "4,0 м3/ч",
            "5 м3/час",
            "5,5 м3/час",
            "500 л/час",
            "53 л/мин",
            "6 м3/час",
            "6000 л/час",
            "7 м3/час",
            "7,2 м3/час",
            "7,3 м3/час",
            "7.6 м3/час",
            "73.3 л/мин",
            "8 м3/час",
            "800 л/час",
            "до 1100 л/сут.",
            "до 2 л/мин",
            "до 6,5 м3/ч"
        ),
        "clean_type" => array(
            "Обратный осмос",
            "Обратный осмос, сорбция, умягчение",
            "Сорбция",
            "Ультрафиолетовое излучение"
        ),
        "clean_steps" => array(
            "3",
            "4",
            "5"
        ),
        "height" => array(
            "40 см",
            "44 см",
            "61 см",
            "94 см",
            "110 см", // "  "
            "125 см",
            "138 см",
            "140 см",
            "150 см",
            "160 см",
            "178 см",
            "179 см",
            "180 мм",
            "310 мм",
            "570 мм"
        ),
        "length" => array(
            "26 см",
            "30 см",
            "40 см"
			
        ),
        "width" => array(
            "55 см",
            "60 см",
            "65 см",
            "80 см",
            "110 см",
            "120 см",
            "150 см"
        ),
        "diameter" => array(
            "21 см",
            "26 см",
            "33 см",
            "37 см",
            "41 см",
            "130 мм",
            "140 мм",
            "170 мм"
        ),
        "dimensions" => array(
            "140x158 мм",
            "158x180 мм",
            "170x449 мм",
            "179x180 мм",
            "20,8x59,5 см",
            "20,8x76,2 см",
            "24x8x8 см",
            "28x8x8 см",
            "29.2x55x47 см",
            "29x55x47 см",
            "38x77,5x66 см", // 38x77,5x66 см
            "56x8x8 см",
            "57x125 см",
            "57x129 см",
            "62x139 см",
            "62x150 см",
            "62x154 см",
            "69x154 см",
            "70x45x16 см",
            "70x70x16 см",
            "78x29x70 см",
            "78x45x29 см",
            "81x153 см",
            "86x180 см",
            "90x17x9 см",
            "93x171 см"
        )
    );

    // public function __construct() {
    // 	foreach ($this->caption as $key => $feature) {
    // 		$query = $this->db->placehold("SELECT * FROM `__features` WHERE name = '$feature'");
    // 		$this->db->query($query);
    // 		$resObject = $this->db->results();
    // 		$id = $resObject[0]->id;

    // 		$query = $this->db->placehold("SELECT value FROM `__options` WHERE feature_id = $id GROUP BY value");
    // 		$this->db->query($query);
    // 		// echo $query;
    // 		$resObject = $this->db->results();

    // 		for($i=0; $i < count($resObject); $i++) {
    // 			$row[] = $resObject[$i]->value;
    // 		}
    // 		$this->filter[$key] = $row;
    // 	}
    // }

    public function getCaptionByCode($code)
    {
        if (isset($this->caption[$code])) {
            $res = $this->caption[$code];
            return $res;
        }
        return false;
    }

    /*
	*
	* Функция возвращает массив количества товаров в бренде
	*
	*/
    public function get_brand_items_count($params)
    {
        $brand = $params["brand"];
        if (!$brand) {
            return false;
        }
        $brand = intval($brand);

        // Выбираем количество товаров из бренда бренды
        $query = $this->db->placehold("SELECT COUNT(*) count FROM `__products` WHERE brand_id = $brand");
        $this->db->query($query);

        return $this->db->results()[0];
    }

    /*
    *
    * Функция возвращает массив минимальной и максимальной цены
    *
    */
    public function get_min_max_price()
    {
        $query = $this->db->placehold("SELECT MIN(price) min, MAX(price) max FROM `__variants`");
        $this->db->query($query);

        $max = intval($this->db->results()[0]->max);
        $min = intval($this->db->results()[0]->min);
        $result = (object)array("min" => $min, "max" => $max);
        return $result;
    }

    public function get_prices()
    {
        $result = "";
        $query = $this->db->placehold("SELECT price FROM `__variants` GROUP BY price");
        $this->db->query($query);
        $prices = $this->db->results();
        // echo "<br>".count($prices)."<br>";
        for ($i = 0; $i < count($prices); $i++) {
            $price = $prices[$i];
            $separator = ",";
            if ($i + 1 == count($prices)) {
                $separator = "";
            }
            $result .= $price->price . $separator;
        }
        $result = (object)array("prices" => $result);
        // print_r($result);
        return $result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    /*
    *
    * Функция возвращает количество товаров по названию категории
    * !Не проверено на подкатегории!
    *
    */
    public function get_count_products_by_parent($params)
    {
        $result = array();
        $ids = array();
        $parent = htmlspecialchars($params["parent"]);
        $query = $this->db->placehold("SELECT id, url FROM `__categories` WHERE name = '$parent'");
        $this->db->query($query);
        $resObject = $this->db->results()[0];
        $result["url"] = $resObject->url;
        $id = $resObject->id;
        array_push($ids, $id);

        $array = array();
        $array = $this->get_categories_id($id);
        $ids = array_merge($ids, $array);

        // оборачивание в массив для написания функции, которую можно будет вызывать из шаблона
        // указав там массив идентификаторов категорий
        $params = array("ids" => $ids);
        $result["count"] = $this->get_count_by_ids($params);
        // print_r($result);
        return (object)$result;
    }

    /*
    *
    * Функция возвращает количество товаров в запрашиваемых категориях
    *
    */
    public function get_count_by_ids($params)
    {
        $ids = $params["ids"];
        if (!is_array($ids)) {
            return false;
        }

        $strIds = implode(', ', $ids);
        // $query = $this->db->placehold("SELECT COUNT(*) count FROM `__products_categories` WHERE category_id IN ($strIds)");
        $query = $this->db->placehold("SELECT COUNT(DISTINCT product_id) count FROM `__products_categories` pc INNER JOIN `__products` p ON pc.product_id = p.id WHERE category_id IN ($strIds) AND p.visible = 1");
        $this->db->query($query);

        return $this->db->results()[0]->count;
    }

    /*
    *
    * Функция возвращает массив идентификаторов подкатегорий
    * Предназначена только для выхова в классе
    *
    */
    public function get_categories_id($id)
    {
        $arrayRes = array();
        // Выбираем количество товаров из бренда бренды
        $query = $this->db->placehold("SELECT id FROM `__categories` WHERE parent_id = $id");
        $this->db->query($query);

        $result = $this->db->results();
        if (!empty($result)) {
            foreach ($result as $result_row) {
                $id = $result_row->id;
                array_push($arrayRes, $id);
                $array = $this->get_categories_id($id);
                $arrayRes = array_merge($arrayRes, $array);
            }
        }
        return $arrayRes;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    /*
    *
    * Функция возвращает количество товаров по атрибуту
    *
    */
    public function get_count_products_by_option($params)
    {
        $result = array();
        $option = htmlspecialchars($params["option"]);
        $query = $this->db->placehold("SELECT COUNT(*) count FROM `__options` WHERE value = '$option'");
        $this->db->query($query);
        $resObject = $this->db->results()[0];
        $result["count"] = $resObject->count;
        return (object)$result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    /*
    *
    * Функция фильтрации по параметру
    *
    */
    public function getOptionByUri($uri, $params)
    {
        $filter = $this->filter;
        $urls = $this->urls;

        if (isset($params["option"])) {
            $option = $params["option"];
            $arUri = explode('/', $uri);

            $filter = $filter[$option];
            $urls = $urls[$option];
            $needle = $arUri[2];
            if (count($arUri) == 4) {
                $needle .= "/" . $arUri[3];
            }
            $index = array_search($needle, $urls);
            return ($index !== false) ? $filter[$index] : false;
        }
        return false;
    }


    private function getByKey($array, $key)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ("'" . $array[$i]->value . "'" == $key) {
                return $array[$i];
            }
        }
    }

    /*
    *
    * Функция возвращает количество товаров по атрибутам
    *
    */
    public function get_count_products_global_OLD()
    {
        $resultAll = array();

        $filters = $this->filter;
        $anchors = $this->anchors;
        $urls = $this->urls;
        $captions = $this->caption;

        foreach ($filters as $key => $filter) {
            $filter = array_map(
                function ($el) {
                    return "'" . $el . "'";
                },
                $filter
            );

            // $urls = $urls[$key];

            // print_r($urls[$key]);
            // echo "<br><br><br><br><br>";

            $anchors_cur = $anchors[$key];
            // print_r($filters);
            // echo $key;
            if (empty($anchors_cur)) {
                $anchors_cur = $filters[$key];
            }

            // feature id
            $feature = $captions[$key];
            $query = $this->db->placehold("SELECT * FROM `__features` WHERE name = '$feature'");
            $this->db->query($query);
            $resObject = $this->db->results();
            $id = $resObject[0]->id;
            $options = implode(',', $filter);
            $query = $this->db->placehold("SELECT COUNT(*) count, value, feature_id FROM `__options` WHERE feature_id = $id AND value IN ($options) GROUP BY value");
            $this->db->query($query);
//			echo $query;

            $resObject = $this->db->results();
            $resCount = count($resObject);
//			 echo $resCount . " " . count($anchors);

            if ($resCount < count($anchors_cur)) {
                // возможно какая-то категория без товаров
                // print_r($resObject);
                // print_r($anchors_cur);
                continue;
//				die($key . $resCount . " " . count($anchors_cur));
            } else {
                $result["name"] = $captions[$key];
                // $result["counts"] = $resObject;
                // $result["urls"] = $urls;
                // $result["anchors"] = $anchors_cur;

                $items = array();
                for ($i = 0; $i < count($resObject); $i++) {
                    $filetrKey = $filter[$i];
                    // echo $filetrKey;
                    $arrayRow = $this->getByKey($resObject, $filetrKey);
                    // print_r($arrayRow);

                    $obj["count"] = $arrayRow->count;
                    $obj["feature_id"] = $arrayRow->feature_id;
                    $obj["feature_id"] = $arrayRow->feature_id;
                    $obj["url"] = $urls[$key][$i];
                    $obj["name"] = $anchors_cur[$i];
                    $items[] = (object)$obj;
                }
                $result["items"] = $items;

                $resultAll[] = (object)$result;
            }
        }

        // print_r($resultAll);
        // die('123');

        return $resultAll;
    }

    public function get_count_products_global($filter=array())
    {
        $resultAll = array();

        $features = $this->features->get_features($filter);

        foreach ($features as $key => $feature) {
            // feature id
            $id = $feature->id;
            $query = $this->db->placehold("SELECT COUNT(*) count, value, feature_id FROM `__options` WHERE feature_id = $id GROUP BY value");
            $this->db->query($query);
//			echo $query;
            $resObject = $this->db->results();
            $resCount = count($resObject);
//			 echo $resCount . " " . count($anchors);

            if ($resCount) {
                $result["name"] = $feature->name;

                $items = array();
                foreach ($resObject as $item) {
                    $obj["count"] = $item->count;
                    $obj["feature_id"] = $item->feature_id;
                    $obj["value"] = $item->value;
//                    $obj["url"] = $urls[$key][$i];
//                    $obj["name"] = $anchors_cur[$i];
                    $items[] = (object)$obj;
                }
                $result["items"] = $items;

                $resultAll[] = (object)$result;
            }
        }

        // print_r($resultAll);
        // die('123');

        return $resultAll;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function getMeta($params)
    {
        $uri = $_SERVER["REQUEST_URI"];
        $uri = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
        $type = $params['type'] ? $params['type'] : "";
        $startPrice = isset($params['startPrice']) ? $params['startPrice'] : "";
        $finishPrice = isset($params['finishPrice']) ? $params['finishPrice'] : "";
        $keyw_template = isset($params['keyw_template']) ? $params['keyw_template'] : "";
        $desc_template = isset($params['desc_template']) ? $params['desc_template'] : "";

        switch ($type) {
            case "roubles":
                $res['meta_title'] = "Фильтры для воды в ценовом диапазоне от $startPrice до $finishPrice рублей – купить в интернет-магазине Уютный дом";
                $res['meta_description'] = "В данном разделе интернет-магазина Уютный дом представлены фильтры для воды и системы водоочистки стоимостью от $startPrice до $finishPrice рублей";
                $res['meta_keywords'] = "стоимость, цена, рубль, фильтр, вода, система, водоочистка, $startPrice, $finishPrice";
                break;
            case "temperature":
                $resTmp = $this->getMetaTemperature($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "pressure":
                $resTmp = $this->getMetaPressure($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "material":
                $resTmp = $this->getMetaMaterial($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "disinfection":
                $resTmp = $this->getMetaDisinfection($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "ecoferox":
                $resTmp = $this->getMetaEcoferox($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "ecotar":
                $resTmp = $this->getMetaEcotar($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "volume":
                $resTmp = $this->getMetaVolume($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "volume_water":
                $resTmp = $this->getMetaVolumeWater($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "volume_column":
                $resTmp = $this->getMetaVolumeColumn($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "separate":
                $resTmp = $this->getMetaSeparate($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "pump":
                $resTmp = $this->getMetaPump($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "power":
                $resTmp = $this->getMetaPower($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "size":
                $resTmp = $this->getMetaSize($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "size_connection":
                $resTmp = $this->getMetaSizeConnection($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "salt":
                $resTmp = $this->getMetaSalt($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "reset":
                $resTmp = $this->getMetaReset($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "speed":
                $resTmp = $this->getMetaSpeed($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "clean_type":
                $resTmp = $this->getMetaCleanType($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "clean_steps":
                $resTmp = $this->getMetaCleanSteps($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "height":
                $resTmp = $this->getMetaHeight($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "length":
                $resTmp = $this->getMetaLength($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "width":
                $resTmp = $this->getMetaWidth($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "diameter":
                $resTmp = $this->getMetaDiameter($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "dimensions":
                $resTmp = $this->getMetaDimensions($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
            case "water":
                $resTmp = $this->getMetaWater($uri, $desc_template, $keyw_template);
                $res['meta_title'] = $resTmp['meta_title'];
                $res['meta_description'] = $resTmp['meta_description'];
                $res['meta_keywords'] = $resTmp['meta_keywords'];
                $res['pageName'] = $resTmp['pageName'];
                break;
        }

        return $res ? $res : false;
    }

    private function getMetaWater($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-dlja-holodnoj-vody":
                $meta_title = "Фильтры для холодной воды: купить в Санкт-Петербурге и Москве, цены в каталоге интернет-магазина Уютный дом";
                $meta_description = "Предлагаем широкий ассортимент фильтров для холодной воды для дачи или коттеджа.  У нас Вы можете купить фильтры по низким ценам!";
                $meta_keywords = "фильтр, холодная, вода";
                $pageName = "Фильтры для холодной воды";
                break;
            case "/catalog/filtry-dlja-gorjachej-vody":
                $meta_title = "Фильтры для горячей воды - цены в интернет-магазине Уютный дом";
                $meta_description = "В данном разделе вы можете выбрать и купить фильтры для горячей воды. Оперативная доставка от интернет-магазина Уютный дом";
                $meta_keywords = "фильтр, горячая, вода";
                $pageName = "Фильтры для горячей воды";
                break;
            case "/catalog/filtry-dlja-holodnoj-i-gorjachej-vody":
                $meta_title = "Купить фильтры для горячей и холодной воды в интернет-магазине Уютный дом";
                $meta_description = "Каталог фильтров для горячей и холодной воды от интернет-магазина Уютный дом. Выгодные цены. Большой ассортимент";
                $meta_keywords = "фильтр, горячая, холодная, вода";
                $pageName = "Фильтры для холодной и горячей воды";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaTemperature($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody+45C":
                $meta_title = "Фильтры с максимальной температурой воды +45 С – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды +45°С";
                $meta_keywords = $keyw_template . "+45 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-1-40C":
                $meta_title = "Фильтры с максимальной температурой воды 1-40 С: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 1-40°С";
                $meta_keywords = $keyw_template . "1-40 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-100C":
                $meta_title = "Фильтры с максимальной температурой воды 100 С: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 100°С";
                $meta_keywords = $keyw_template . "100 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-2-40C":
                $meta_title = "Фильтры с максимальной температурой воды 2-40 С – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 2-40°С";
                $meta_keywords = $keyw_template . "2-40 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-2-50C":
                $meta_title = "Фильтры с максимальной температурой воды 2-50 С – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 2-50°С";
                $meta_keywords = $keyw_template . "2-50 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-30C":
                $meta_title = "Фильтры с максимальной температурой воды 30 C: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 30°С";
                $meta_keywords = $keyw_template . "30 C";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-300C":
                $meta_title = "Фильтры с максимальной температурой воды 300 C в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 300°С";
                $meta_keywords = $keyw_template . "300 C";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-4-40C":
                $meta_title = "Фильтры с максимальной температурой воды 4-40 С: стоимость угольного фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 4-40°С";
                $meta_keywords = $keyw_template . "4-40 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-4-45C":
                $meta_title = "Фильтры с максимальной температурой воды 4-45 C в Санкт-Петербурге и Москве: описание и цена фильтра в интернет-магазине Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 4-45°С";
                $meta_keywords = $keyw_template . "4-45 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-4-95C":
                $meta_title = "Фильтры с максимальной температурой воды 4-95 С – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 4-95°С";
                $meta_keywords = $keyw_template . "4-95 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-40C":
                $meta_title = "Фильтры с максимальной температурой воды 40 С: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 40°С";
                $meta_keywords = $keyw_template . "40 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-45C":
                $meta_title = "Фильтры с максимальной температурой воды 45 С – заказать в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 45°С";
                $meta_keywords = $keyw_template . "45 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-5-25C":
                $meta_title = "Фильтры с максимальной температурой воды 5-25 C в Санкт-Петербурге и Москве: фото и цена – Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 5-25°С";
                $meta_keywords = $keyw_template . "5-25 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-50C":
                $meta_title = "Фильтры с максимальной температурой воды 50 C в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 50°С";
                $meta_keywords = $keyw_template . "50 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-70C":
                $meta_title = "Фильтры с максимальной температурой воды 70 C – купить недорого в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 70°С";
                $meta_keywords = $keyw_template . "70 C";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-80C":
                $meta_title = "Фильтры с максимальной температурой воды 80 C цена в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 80°С";
                $meta_keywords = $keyw_template . "80 C";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-95C":
                $meta_title = "Фильтры с максимальной температурой воды 95 C: цена в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды 95°С";
                $meta_keywords = $keyw_template . "95 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-do-45C":
                $meta_title = "Фильтры с максимальной температурой воды до 45 С – купить фильтр недорого в Санкт-Петербурге и Москве – Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды до 45°С";
                $meta_keywords = $keyw_template . "до 45 С";
                break;
            case "/catalog/filtry-s-maksimalnoj-temperaturoj-vody-do-70C":
                $meta_title = "Фильтры с максимальной температурой воды до 70 C – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В данном разделе интернет-магазина Уютный Дом представлены фильтры с максимальной температурой воды до 70°С";
                $meta_keywords = $keyw_template . "до 70 C";
                $pageName = "";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaPressure($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-1-7atm":
                $meta_title = "Фильтры с максимальным давлением воды 1-7 атм. – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = $desc_template . "1-7 атм.";
                $meta_keywords = $keyw_template . "1-7 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-4atm":
                $meta_title = "Фильтры с максимальным давлением воды 4 атм.: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "4 атм.";
                $meta_keywords = $keyw_template . "4 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-6atm":
                $meta_title = "Фильтры с максимальным давлением воды 6 атм.: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = $desc_template . "6 атм.";
                $meta_keywords = $keyw_template . "6 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-7atm":
                $meta_title = "Фильтры с максимальным давлением воды 7 атм. – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "7 атм.";
                $meta_keywords = $keyw_template . "7 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-8atm":
                $meta_title = "Фильтры с максимальным давлением воды 8 атм. – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "8 атм.";
                $meta_keywords = $keyw_template . "8 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-10atm":
                $meta_title = "Фильтры с максимальным давлением воды 10 атм. – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "10 атм.";
                $meta_keywords = $keyw_template . "10 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-16atm":
                $meta_title = "Фильтры с максимальным давлением воды 16 атм. – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "16 атм.";
                $meta_keywords = $keyw_template . "16 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-do-6atm":
                $meta_title = "Фильтры с максимальным давлением воды до 6 атм.: стоимость угольного фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "до 6 атм.";
                $meta_keywords = $keyw_template . "до 6 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-do-8atm":
                $meta_title = "Фильтры с максимальным давлением воды до 8 атм.: стоимость угольного фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "до 8 атм.";
                $meta_keywords = $keyw_template . "до 8 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-do-25atm":
                $meta_title = "Фильтры с максимальным давлением воды до 25 атм.: стоимость угольного фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "до 25 атм.";
                $meta_keywords = $keyw_template . "до 25 атм.";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-15bar":
                $meta_title = "Фильтры с максимальным давлением воды 15 бар: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "15 бар";
                $meta_keywords = $keyw_template . "15 бар";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-do-25bar":
                $meta_title = "Фильтры с максимальным давлением воды до 25 бар – заказать в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "до 25 бар";
                $meta_keywords = $keyw_template . "до 25 бар";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-1,6MPa":
                $meta_title = "Фильтры с максимальным давлением воды 1,6 МПа в Санкт-Петербурге и Москве: фото и цена – Уютный дом";
                $meta_description = $desc_template . "1,6 МПа";
                $meta_keywords = $keyw_template . "1,6 МПа";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-2,5Mpa":
                $meta_title = "Фильтры с максимальным давлением воды 2,5 МПа в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = $desc_template . "2,5 МПа";
                $meta_keywords = $keyw_template . "2,5 МПа";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-do-2,5Mpa":
                $meta_title = "Фильтры с максимальным давлением воды до 2,5 МПа – купить недорого в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "до 2,5 МПа";
                $meta_keywords = $keyw_template . "до 2,5 МПа";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-PN16":
                $meta_title = "Фильтры с максимальным давлением воды PN16 цена в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "PN16";
                $meta_keywords = $keyw_template . "PN16";
                break;
            case "/catalog/filtry-s-maksimalnym-davleniem-vody-PN25":
                $meta_title = "Фильтры с максимальным давлением воды PN25 цена в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = $desc_template . "PN25";
                $meta_keywords = $keyw_template . "PN25";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaMaterial($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-materialom-korpusa-nerzhavejushhaja-stal":
                $meta_title = "Фильтры с материалом корпуса нержавеющая сталь – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры по материалу корпуса. Здесь мы подобрали для вас фильтры из нержавеющей стали";
                $meta_keywords = "нержавеющая, сталь, материал, корпус, фильтр";
                break;
            case "/catalog/filtry-s-materialom-korpusa-nerzhavejushhaja-stal-plastik-steklovolokno":
                $meta_title = "Фильтры с материалом корпуса нержавеющая сталь, пластик, стекловолокно: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры по материалу корпуса. Здесь мы подобрали для вас фильтры из нержавеющей стали, пластика, стекловолокна";
                $meta_keywords = "нержавеющая, сталь, пластик, стекловолокно, материал, корпус, фильтр";
                break;
            case "/catalog/filtry-s-materialom-korpusa-pishhevoj-plastik":
                $meta_title = "Фильтры с материалом корпуса пищевой пластик: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры по материалу корпуса. Здесь мы подобрали для вас фильтры из пищевого пластика";
                $meta_keywords = "пищевой, пластик, материал, корпус, фильтр";
                break;
            case "/catalog/filtry-s-materialom-korpusa-plastik-steklovolokno":
                $meta_title = "Фильтры с материалом корпуса пластик, стекловолокно – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры по материалу корпуса. Здесь мы подобрали для вас фильтры из пластика, стекловолокна";
                $meta_keywords = "пластик, стекловолокно, материал, корпус, фильтр";
                break;
            case "/catalog/filtry-s-materialom-korpusa-steklovolokno":
                $meta_title = "Фильтры с материалом корпуса стекловолокно – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры по материалу корпуса. Здесь мы подобрали для вас фильтры из стекловолокна";
                $meta_keywords = "стекловолокно, материал, корпус, фильтр";
                break;
            case "/catalog/filtry-s-materialom-korpusa-steklovolokno-pishhevoj-plastik":
                $meta_title = "Фильтры с материалом корпуса стекловолокно, пищевой пластик: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры по материалу корпуса. Здесь мы подобрали для вас фильтры из стекловолокна, пищевого пластика";
                $meta_keywords = "стекловолокно, пищевой, пластик, материал, корпус, фильтр";
                break;
            case "/catalog/filtry-s-materialom-korpusa-tehnicheskij-plastik":
                $meta_title = "Фильтры с материалом корпуса технический пластик в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры по материалу корпуса. Здесь мы подобрали для вас фильтры из технического пластика";
                $meta_keywords = "технический, пластик, материал, корпус, фильтр";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaDisinfection($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-obezzarazhivaniem":
                $meta_title = "Фильтры с обеззараживанием – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с обеззараживанием.";
                $meta_keywords = "фильтр, обеззараживание";
                break;
            case "/catalog/filtry-bez-obezzarazhivaniya":
                $meta_title = "Фильтры без обеззараживания: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры без обеззараживания";
                $meta_keywords = "фильтр, без, обеззараживание";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaEcoferox($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-1-meshok":
                $meta_title = "Фильтры с загрузкой обезжелезивания EcoFerox (Экоферокс) 1 мешок – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с загрузкой обезжелезивания Экоферокс 1 мешок";
                $meta_keywords = "фильтр, загрузка, ecoferox, экоферокс, 1, мешок";
                break;
            case "/catalog/filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-2-meshka":
                $meta_title = "Фильтры с загрузкой обезжелезивания EcoFerox (Экоферокс) 2 мешка: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с загрузкой обезжелезивания Экоферокс 2 мешка";
                $meta_keywords = "фильтр, загрузка, ecoferox, экоферокс, 2, мешок";
                break;
            case "/catalog/filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-3-meshka":
                $meta_title = "Фильтры с загрузкой обезжелезивания EcoFerox (Экоферокс) 3 мешка: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с загрузкой обезжелезивания Экоферокс 3 мешка";
                $meta_keywords = "фильтр, загрузка, ecoferox, экоферокс, 3, мешок";
                break;
            case "/catalog/filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-4-meshka":
                $meta_title = "Фильтры с загрузкой обезжелезивания EcoFerox (Экоферокс) 4 мешка – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с загрузкой обезжелезивания Экоферокс 4 мешка";
                $meta_keywords = "фильтр, загрузка, ecoferox, экоферокс, 4, мешок";
                break;
            case "/catalog/filtry-s-zagruzkoy-obezzhelezivanija-ecoferox-5-meshkov":
                $meta_title = "Фильтры с загрузкой обезжелезивания EcoFerox (Экоферокс) 5 мешков – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с загрузкой обезжелезивания Экоферокс 5 мешков";
                $meta_keywords = "фильтр, загрузка, ecoferox, экоферокс, 5, мешок";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaEcotar($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-zagruzkoy-ecotar-1-meshok":
                $meta_title = "Фильтры с загрузкой Ecotar (Экотар) 1 мешок – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с загрузкой Экотар 1 мешок";
                $meta_keywords = "фильтр, загрузка, ecotar, экотар, 1, мешок";
                break;
            case "/catalog/filtry-s-zagruzkoy-ecotar-1.5-meshka":
                $meta_title = "Фильтры с загрузкой Ecotar (Экотар) 1,5 мешка: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с загрузкой Экотар 1,5 мешка";
                $meta_keywords = "фильтр, загрузка, ecotar, экотар, 1,5, мешок";
                break;
            case "/catalog/filtry-s-zagruzkoy-ecotar-2-meshka":
                $meta_title = "Фильтры с загрузкой Ecotar (Экотар) 2 мешка: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с загрузкой Экотар 2 мешка";
                $meta_keywords = "фильтр, загрузка, ecotar, экотар, 2, мешок";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaVolume($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-nakopitelnoy-emkostu-obyemom-5l":
                $meta_title = "Фильтры с накопительной емкостью 5 литров – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры с накопительной емкостью 5 л";
                $meta_keywords = "фильтр, накопительная, емкость, объем, 5, л";
                break;
            case "/catalog/filtry-s-nakopitelnoy-emkostu-obyemom-12l":
                $meta_title = "Фильтры с накопительной емкостью 12 литров: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры с накопительной емкостью 12 л";
                $meta_keywords = "фильтр, накопительная, емкость, объем, 12, л";
                break;
            case "/catalog/filtry-s-nakopitelnoy-emkostu-obyemom-40l":
                $meta_title = "Фильтры с накопительной емкостью 40 литров: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры с накопительной емкостью 40 л";
                $meta_keywords = "фильтр, накопительная, емкость, объем, 40, л";
                break;
            case "/catalog/filtry-s-nakopitelnoy-emkostu-obyemom-net":
            case "/catalog/filtry-bez-nakopitelnoy-emkosti":
                $meta_title = "Фильтры без накопительной емкости – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры без накопительной емкости";
                $meta_keywords = "фильтр, накопительная, емкость, объем, нет";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaVolumeWater($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-obyemom-vody-na-regeneraciju-36l":
                $meta_title = "Фильтры с объемом воды на регенерацию 36 л – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом воды на регенерацию 36 л.";
                $meta_keywords = "фильтр, объем, вода, регенерация, 36 л";
                break;
            case "/catalog/filtry-s-obyemom-vody-na-regeneraciju-60l":
                $meta_title = "Фильтры с объемом воды на регенерацию 60 л: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом воды на регенерацию 60 л.";
                $meta_keywords = "фильтр, объем, вода, регенерация, 60 л";
                break;
            case "/catalog/filtry-s-obyemom-vody-na-regeneraciju-80-120l":
                $meta_title = "Фильтры с объемом воды на регенерацию 80-120 л: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом воды на регенерацию 80-120 л.";
                $meta_keywords = "фильтр, объем, вода, регенерация, 80-120 л";
                break;
            case "/catalog/filtry-s-obyemom-vody-na-regeneraciju-100l":
                $meta_title = "Фильтры с объемом воды на регенерацию 100 л – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом воды на регенерацию 100 л.";
                $meta_keywords = "фильтр, объем, вода, регенерация, 100 л";
                break;
            case "/catalog/filtry-s-obyemom-vody-na-regeneraciju-150l":
                $meta_title = "Фильтры с объемом воды на регенерацию 150 л – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом воды на регенерацию 150 л.";
                $meta_keywords = "фильтр, объем, вода, регенерация, 150 л";
                break;
            case "/catalog/filtry-s-obyemom-vody-na-regeneraciju-220l":
                $meta_title = "Фильтры с объемом воды на регенерацию 220 л: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом воды на регенерацию 220 л.";
                $meta_keywords = "фильтр, объем, вода, регенерация, 220 л";
                break;
            case "/catalog/filtry-s-obyemom-vody-na-regeneraciju-300l":
                $meta_title = "Фильтры с объемом воды на регенерацию 300 л в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом воды на регенерацию 300 л.";
                $meta_keywords = "фильтр, объем, вода, регенерация, 300 л";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaVolumeColumn($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-obyemom-kolonny-11l":
                $meta_title = "Фильтры с объемом колонны 11 л – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 11 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 11 л";
                break;
            case "/catalog/filtry-s-obyemom-kolonny-21l":
                $meta_title = "Фильтры с объемом колонны 21 л: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 21 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 21 л";
                break;
            case "/catalog/filtry-s-obyemom-kolonny-33l":
                $meta_title = "Фильтры с объемом колонны 33 л: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 33 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 33 л";
                break;
            case "/catalog/filtry-s-obyemom-kolonny-33.6l":
                $meta_title = "Фильтры с объемом колонны 33,6 л – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 33,6 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 33,6 л";
                break;
            case "/catalog/filtry-s-obyemom-kolonny-60l":
                $meta_title = "Фильтры с объемом колонны 60 л – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 60 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 60 л";
                break;
            case "/catalog/filtry-s-obyemom-kolonny-61l":
                $meta_title = "Фильтры с объемом колонны 61 л: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 61 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 61 л";
                break;
            case "/catalog/filtry-s-obyemom-kolonny-103l":
                $meta_title = "Фильтры с объемом колонны 103 л в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 103 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 103 л";
                break;
            case "/catalog/filtry-s-obyemom-kolonny-105l":
                $meta_title = "Фильтры с объемом колонны 105 л – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 105 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 105 л";
                break;
            case "/catalog/filtry-s-obyemom-kolonny-140l":
                $meta_title = "Фильтры с объемом колонны 140 л: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 140 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 140 л";
                break;
            case "/catalog/filtry-s-obyemom-kolonny-170l":
                $meta_title = "Фильтры с объемом колонны 170 л: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с объемом колонны 170 литров.";
                $meta_keywords = "фильтр, объем, вода, колонна, 170 л";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaSeparate($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-otdelnym-kranom-obratniy-osmos":
                $meta_title = "Фильтры с отдельным краном (обратный осмос) – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с отдельным краном (обратный осмос)";
                $meta_keywords = "фильтр, отдельный, кран, обратный, осмос";
                break;
            case "/catalog/filtry-s-otdelnym-kranom":
                $meta_title = "Фильтры с отдельным краном: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с отдельным краном";
                $meta_keywords = "фильтр, отдельный, кран, есть";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaPump($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-pompoy-dlja-povyshenija-davlenija":
                $meta_title = "Фильтры с помпой для повышения давления – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с помпой для повышения давления";
                $meta_keywords = "фильтр, помпа, повышение, давление";
                break;
            case "/catalog/filtry-bez-pompy-dlja-povyshenija-davlenija":
                $meta_title = "Фильтры без помпы для повышения давления: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры без помпы для повышения давления";
                $meta_keywords = "фильтр, без, помпа, повышение, давление";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaPower($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-potrebljaemoj-moshhnostju-0.8kvt":
                $meta_title = "Фильтры с потребляемой мощностью 0,8 кВт – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с потребляемой мощностью 0,8 кВт";
                $meta_keywords = "фильтр, потребляемая, мощность, 0,8 кВт";
                break;
            case "/catalog/filtry-s-potrebljaemoj-moshhnostju-120w":
                $meta_title = "Фильтры с потребляемой мощностью 120 Вт: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с потребляемой мощностью 120 Вт";
                $meta_keywords = "фильтр, потребляемая, мощность, 120 Вт";
                break;
            case "/catalog/filtry-s-potrebljaemoj-moshhnostju-190w":
                $meta_title = "Фильтры с потребляемой мощностью 190 Вт: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с потребляемой мощностью 190 Вт";
                $meta_keywords = "фильтр, потребляемая, мощность, 190 Вт";
                break;
            case "/catalog/filtry-s-potrebljaemoj-moshhnostju-200w":
                $meta_title = "Фильтры с потребляемой мощностью 200 Вт – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с потребляемой мощностью 200 Вт";
                $meta_keywords = "фильтр, потребляемая, мощность, 200 Вт";
                break;
            case "/catalog/filtry-s-potrebljaemoj-moshhnostju-220v":
                $meta_title = "Фильтры с потребляемой мощностью 220 В – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с потребляемой мощностью 220 В";
                $meta_keywords = "фильтр, потребляемая, мощность, 220 В";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaSize($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-razmer-kartridzha-10-big-blue":
                $meta_title = "Фильтры: размер картриджа 10 Big Blue – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с размером картриджа 10 Big Blue";
                $meta_keywords = "фильтр, размер, картридж, 10, big, blue";
                break;
            case "/catalog/filtry-razmer-kartridzha-10-slim-line":
                $meta_title = "Фильтры: размер картриджа 10 Slim Line: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с размером картриджа 10 Slim Line";
                $meta_keywords = "фильтр, размер, картридж, 10, slim, line";
                break;
            case "/catalog/filtry-razmer-kartridzha-20-big-blue":
                $meta_title = "Фильтры: размер картриджа 20 Big Blue: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с размером картриджа 20 Big Blue";
                $meta_keywords = "фильтр, размер, картридж, 20, big, blue";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaSizeConnection($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-parametrom-prisoedinenija-1-1/2":
                $meta_title = "Фильтры с параметром присоединения 1 1/2' – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с парметром присоединения 1 1/2'";
                $meta_keywords = "фильтр, параметр, присоединение, 1 1/2'";
                break;
            case "/catalog/filtry-s-parametrom-prisoedinenija-1-1/4":
                $meta_title = "Фильтры с параметром присоединения 1 1/4': стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с парметром присоединения 1 1/4'";
                $meta_keywords = "фильтр, параметр, присоединение, 1 1/4'";
                break;
            case "/catalog/filtry-s-parametrom-prisoedinenija-1":
                $meta_title = "Фильтры с параметром присоединения 1': цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с парметром присоединения 1'";
                $meta_keywords = "фильтр, параметр, присоединение, 1'";
                break;
            case "/catalog/filtry-s-parametrom-prisoedinenija-1/2":
                $meta_title = "Фильтры с параметром присоединения 1/2' – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с парметром присоединения 1/2'";
                $meta_keywords = "фильтр, параметр, присоединение, 1/2'";
                break;
            case "/catalog/filtry-s-parametrom-prisoedinenija-1/4":
                $meta_title = "Фильтры с параметром присоединения 1/4' – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с парметром присоединения 1/4'";
                $meta_keywords = "фильтр, параметр, присоединение, 1/4'";
                break;
            case "/catalog/filtry-s-parametrom-prisoedinenija-3/4":
                $meta_title = "Фильтры с параметром присоединения 3/4': цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с парметром присоединения 3/4'";
                $meta_keywords = "фильтр, параметр, присоединение, 3/4'";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaSalt($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-rashodom-soli-na-regeneraciju-0.5kg":
                $meta_title = "Фильтры с расходом соли на регенерацию 0,5 кг – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с расходом соли на регенерацию 0,5 кг";
                $meta_keywords = "фильтр, расход, соль, регенерация, 0,5 кг";
                break;
            case "/catalog/filtry-s-rashodom-soli-na-regeneraciju-1kg":
                $meta_title = "Фильтры с расходом соли на регенерацию 1 кг: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с расходом соли на регенерацию 1 кг";
                $meta_keywords = "фильтр, расход, соль, регенерация, 1 кг";
                break;
            case "/catalog/filtry-s-rashodom-soli-na-regeneraciju-1.9kg":
                $meta_title = "Фильтры с расходом соли на регенерацию 1,9 кг: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с расходом соли на регенерацию 1,9 кг";
                $meta_keywords = "фильтр, расход, соль, регенерация, 1,9 кг";
                break;
            case "/catalog/filtry-s-rashodom-soli-na-regeneraciju-2.4kg":
                $meta_title = "Фильтры с расходом соли на регенерацию 2,4 кг – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с расходом соли на регенерацию 2,4 кг";
                $meta_keywords = "фильтр, расход, соль, регенерация, 2,4 кг";
                break;
            case "/catalog/filtry-s-rashodom-soli-na-regeneraciju-3.5kg":
                $meta_title = "Фильтры с расходом соли на регенерацию 3,5 кг – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с расходом соли на регенерацию 3,5 кг";
                $meta_keywords = "фильтр, расход, соль, регенерация, 3,5 кг";
                break;
            case "/catalog/filtry-s-rashodom-soli-na-regeneraciju-5kg":
                $meta_title = "Фильтры с расходом соли на регенерацию 5 кг: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с расходом соли на регенерацию 5 кг";
                $meta_keywords = "фильтр, расход, соль, регенерация, 5 кг";
                break;
            case "/catalog/filtry-s-rashodom-soli-na-regeneraciju-8.5kg":
                $meta_title = "Фильтры с расходом соли на регенерацию 8,5 кг в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры с расходом соли на регенерацию 8,5 кг";
                $meta_keywords = "фильтр, расход, соль, регенерация, 8,5 кг";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaReset($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-sbros-pri-promyvke-12l":
                $meta_title = "Фильтры: сброс при промывке 12 л – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 12 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 12 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-20l":
                $meta_title = "Фильтры: сброс при промывке 20 л: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 20 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 20 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-60l":
                $meta_title = "Фильтры: сброс при промывке 60 л: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 60 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 60 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-100l":
                $meta_title = "Фильтры: сброс при промывке 100 л – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 100 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 100 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-150l":
                $meta_title = "Фильтры: сброс при промывке 150 л – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 150 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 150 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-220l":
                $meta_title = "Фильтры: сброс при промывке 220 л: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 220 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 220 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-250l":
                $meta_title = "Фильтры: сброс при промывке 250 л в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 250 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 250 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-300l":
                $meta_title = "Фильтры: сброс при промывке 300 л – купить фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 300 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 300 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-320l":
                $meta_title = "Фильтры: сброс при промывке 320 л – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 320 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 320 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-350l":
                $meta_title = "Фильтры: сброс при промывке 350 л: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 350 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 350 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-400l":
                $meta_title = "Фильтры: сброс при промывке 400 л: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 400 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 400 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-450l":
                $meta_title = "Фильтры: сброс при промывке 450 л – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке 450 литров.";
                $meta_keywords = "фильтр, сброс, промывка, 450 л";
                break;
            case "/catalog/filtry-sbros-pri-promyvke-ot-320l":
                $meta_title = "Фильтры: сброс при промывке от 320 л: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В этом разделе интернет-магазина Уютный Дом представлены фильтры, сброс при промывке от 320 литров.";
                $meta_keywords = "фильтр, сброс, промывка, от 320 л";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaSpeed($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-skorost-filtracii-0.14lmin":
                $meta_title = "Фильтры со скоростью фильтрации 0.14 л/мин – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 0.14 л/мин";
                $meta_keywords = "фильтр, скорость, фильтрация, 0.14 л/мин";
                break;
            case "/catalog/filtry-skorost-filtracii-0.25mh":
                $meta_title = "Фильтры со скоростью фильтрации 0.25 м3/час: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 0.25 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 0.25 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-0.26lmin":
                $meta_title = "Фильтры со скоростью фильтрации 0.26 л/мин: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 0.26 л/мин";
                $meta_keywords = "фильтр, скорость, фильтрация, 0.26 л/мин";
                break;
            case "/catalog/filtry-skorost-filtracii-0.5m3h":
                $meta_title = "Фильтры со скоростью фильтрации 0.5 м3/час – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 0.5 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 0.5 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-0.6m3h":
                $meta_title = "Фильтры со скоростью фильтрации 0.6 м3/час – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 0.6 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 0.6 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-0.8m3h":
                $meta_title = "Фильтры со скоростью фильтрации 0.8 м3/час: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 0.8 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 0.8 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-0.9m3h":
                $meta_title = "Фильтры со скоростью фильтрации 0.9 м3/час в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 0.9 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 0.9 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-1m3h":
                $meta_title = "Фильтры со скоростью фильтрации 1 м3/час – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 1 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 1 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-1.5m3h":
                $meta_title = "Фильтры со скоростью фильтрации 1,5 м3/час: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 1,5 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 1,5 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-1.2m3h":
                $meta_title = "Фильтры со скоростью фильтрации 1,2 м3/час: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 1,2 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 1,2 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-1.8m3h":
                $meta_title = "Фильтры со скоростью фильтрации 1,8 м3/час – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 1,8 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 1,8 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-10m3h":
                $meta_title = "Фильтры со скоростью фильтрации 10 м3/ч: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 10 м3/ч";
                $meta_keywords = "фильтр, скорость, фильтрация, 10 м3/ч";
                break;
            case "/catalog/filtry-skorost-filtracii-1000lh":
                $meta_title = "Фильтры со скоростью фильтрации 1000 л/час: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 1000 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 1000 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-1140ld":
                $meta_title = "Фильтры со скоростью фильтрации 1140 л/сут – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 1140 л/сут.";
                $meta_keywords = "фильтр, скорость, фильтрация, 1140 л/сут.";
                break;
            case "/catalog/filtry-skorost-filtracii-1200lh":
                $meta_title = "Фильтры со скоростью фильтрации 1200 л/час – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 1200 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 1200 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-14m3h":
                $meta_title = "Фильтры со скоростью фильтрации 14 м3/час: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 14 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 14 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-16m3h":
                $meta_title = "Фильтры со скоростью фильтрации 16 м3/час в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 16 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 16 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-17.4llh":
                $meta_title = "Фильтры со скоростью фильтрации 17,4 л/час – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 17,4 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 17,4 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-1800ld":
                $meta_title = "Фильтры со скоростью фильтрации 1800 л/сут: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 1800 л/сут";
                $meta_keywords = "фильтр, скорость, фильтрация, 1800 л/сут";
                break;
            case "/catalog/filtry-skorost-filtracii-1800lh":
                $meta_title = "Фильтры со скоростью фильтрации 1800 л/час: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 1800 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 1800 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-2.5m3h":
                $meta_title = "Фильтры со скоростью фильтрации 2,5 м3/час – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 2,5 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 2,5 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-20lmin":
                $meta_title = "Фильтры со скоростью фильтрации 20 л/мин: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 20 л/мин";
                $meta_keywords = "фильтр, скорость, фильтрация, 20 л/мин";
                break;
            case "/catalog/filtry-skorost-filtracii-200lh":
                $meta_title = "Фильтры со скоростью фильтрации 200 л/час: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 200 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 200 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-250ld":
                $meta_title = "Фильтры со скоростью фильтрации 250 л/сутки – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 250 л/сутки";
                $meta_keywords = "фильтр, скорость, фильтрация, 250 л/сутки";
                break;
            case "/catalog/filtry-skorost-filtracii-2500lh":
                $meta_title = "Фильтры со скоростью фильтрации 2500 л/час – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 2500 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 2500 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-3m3h":
                $meta_title = "Фильтры со скоростью фильтрации 3 м3/ч: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 3 м3/ч";
                $meta_keywords = "фильтр, скорость, фильтрация, 3 м3/ч";
                break;
            case "/catalog/filtry-skorost-filtracii-3.2m3h":
                $meta_title = "Фильтры со скоростью фильтрации 3,2 м3/час в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 3,2 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 3,2 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-3000lh":
                $meta_title = "Фильтры со скоростью фильтрации 3000 л/час – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 3000 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 3000 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-4m3h":
                $meta_title = "Фильтры со скоростью фильтрации 4,0 м3/ч: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 4,0 м3/ч";
                $meta_keywords = "фильтр, скорость, фильтрация, 4,0 м3/ч";
                break;
            case "/catalog/filtry-skorost-filtracii-5m3h":
                $meta_title = "Фильтры со скоростью фильтрации 5 м3/час: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 5 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 5 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-5.5m3h":
                $meta_title = "Фильтры со скоростью фильтрации 5,5 м3/час – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 5,5 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 5,5 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-500lh":
                $meta_title = "Фильтры со скоростью фильтрации 500 л/час: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 500 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 500 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-53lm":
                $meta_title = "Фильтры со скоростью фильтрации 53 л/мин: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 53 л/мин";
                $meta_keywords = "фильтр, скорость, фильтрация, 53 л/мин";
                break;
            case "/catalog/filtry-skorost-filtracii-6m3h":
                $meta_title = "Фильтры со скоростью фильтрации 6 м3/час – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 6 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 6 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-6000lh":
                $meta_title = "Фильтры со скоростью фильтрации 6000 л/час – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 6000 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 6000 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-7m3h":
                $meta_title = "Фильтры со скоростью фильтрации 7 м3/час: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 7 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 7 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-7.2m3h":
                $meta_title = "Фильтры со скоростью фильтрации 7,2 м3/час в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 7,2 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 7,2 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-7.3m3h":
                $meta_title = "Фильтры со скоростью фильтрации 7,3 м3/час – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 7,3 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 7,3 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-7.6m3h":
                $meta_title = "Фильтры со скоростью фильтрации 7.6 м3/час: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 7.6 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 7.6 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-73.3lmin":
                $meta_title = "Фильтры со скоростью фильтрации 73.3 л/мин: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 73.3 л/мин";
                $meta_keywords = "фильтр, скорость, фильтрация, 73.3 л/мин";
                break;
            case "/catalog/filtry-skorost-filtracii-8m3h":
                $meta_title = "Фильтры со скоростью фильтрации 8 м3/час – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 8 м3/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 8 м3/час";
                break;
            case "/catalog/filtry-skorost-filtracii-800lh":
                $meta_title = "Фильтры со скоростью фильтрации 800 л/час: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации 800 л/час";
                $meta_keywords = "фильтр, скорость, фильтрация, 800 л/час";
                break;
            case "/catalog/filtry-skorost-filtracii-do-1100ld":
                $meta_title = "Фильтры со скоростью фильтрации до 1100 л/сут: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации до 1100 л/сут.";
                $meta_keywords = "фильтр, скорость, фильтрация, до 1100 л/сут.";
                break;
            case "/catalog/filtry-skorost-filtracii-do-2lmin":
                $meta_title = "Фильтры со скоростью фильтрации до 2 л/мин – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации до 2 л/мин";
                $meta_keywords = "фильтр, скорость, фильтрация, до 2 л/мин";
                break;
            case "/catalog/filtry-skorost-filtracii-do-6,5m3h":
                $meta_title = "Фильтры со скоростью фильтрации до 6,5 м3/ч – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со скоростью фильтрации до 6,5 м3/ч";
                $meta_keywords = "фильтр, скорость, фильтрация, до 6,5 м3/ч";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaCleanType($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-so-sposobom-ochistki-obratnyj-osmos":
                $meta_title = "Фильтры со способом очистки обратный осмос – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со со способом очистки обратный осмос";
                $meta_keywords = "фильтр, способ, очистка, осмос, обратный";
                break;
            case "/catalog/filtry-so-sposobom-ochistki-obratnyj-osmos-sorbcija-umjagchenie":
                $meta_title = "Фильтры со способом очистки обратный осмос, сорбция, умягчение: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со со способом очистки обратный осмос, сорбция, умягчение";
                $meta_keywords = "фильтр, способ, очистка, осмос, сорбция, умягчение";
                break;
            case "/catalog/filtry-so-sposobom-ochistki-sorbcija":
                $meta_title = "Фильтры со способом очистки сорбция: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со со способом очистки сорбция";
                $meta_keywords = "фильтр, способ, очистка, сорбция";
                break;
            case "/catalog/filtry-so-sposobom-ochistki-ultrafioletovoe-izluchenie":
                $meta_title = "Фильтры со способом очистки ультрафиолетовое излучение – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры со со способом очистки ультрафиолетовое излучение";
                $meta_keywords = "фильтр, способ, очистка, ультрафиолетовое, излучение";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaCleanSteps($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-s-chislom-stupenej-ochistki-3":
                $meta_title = "Фильтры с числом ступеней очистки 3 – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры с числом ступеней очистки 3";
                $meta_keywords = "фильтр, число, ступень, очистка, 3";
                break;
            case "/catalog/filtry-s-chislom-stupenej-ochistki-4":
                $meta_title = "Фильтры с числом ступеней очистки 4: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры с числом ступеней очистки 4";
                $meta_keywords = "фильтр, число, ступень, очистка, 4";
                break;
            case "/catalog/filtry-s-chislom-stupenej-ochistki-5":
                $meta_title = "Фильтры с числом ступеней очистки 5: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры с числом ступеней очистки 5";
                $meta_keywords = "фильтр, число, ступень, очистка, 5";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaHeight($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-dlya-vody-vysotoy-40cm":
                $meta_title = "Фильтры для воды высотой 40 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 40 см";
                $meta_keywords = "фильтр, вода, высота, 40 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-44cm":
                $meta_title = "Фильтры для воды высотой 44 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 44 см";
                $meta_keywords = "фильтр, вода, высота, 44 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-61cm":
                $meta_title = "Фильтры для воды высотой 61 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 61 см";
                $meta_keywords = "фильтр, вода, высота, 61 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-94cm":
                $meta_title = "Фильтры для воды высотой 94 см – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 94 см";
                $meta_keywords = "фильтр, вода, высота, 94 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-110cm":
                $meta_title = "Фильтры для воды высотой 110 см – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 110 см";
                $meta_keywords = "фильтр, вода, высота, 110 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-125cm":
                $meta_title = "Фильтры для воды высотой 125 см: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 125 см";
                $meta_keywords = "фильтр, вода, высота, 125 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-138cm":
                $meta_title = "Фильтры для воды высотой 138 см в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 138 см";
                $meta_keywords = "фильтр, вода, высота, 138 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-140cm":
                $meta_title = "Фильтры для воды высотой 140 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 140 см";
                $meta_keywords = "фильтр, вода, высота, 140 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-150cm":
                $meta_title = "Фильтры для воды высотой 150 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 150 см";
                $meta_keywords = "фильтр, вода, высота, 150 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-160cm":
                $meta_title = "Фильтры для воды высотой 160 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 160 см";
                $meta_keywords = "фильтр, вода, высота, 160 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-178cm":
                $meta_title = "Фильтры для воды высотой 178 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 178 см";
                $meta_keywords = "фильтр, вода, высота, 178 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-179cm":
                $meta_title = "Фильтры для воды высотой 179 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 179 см";
                $meta_keywords = "фильтр, вода, высота, 179 см";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-180mm":
                $meta_title = "Фильтры для воды высотой 180 мм: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 180 мм";
                $meta_keywords = "фильтр, вода, высота, 180 мм";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-310mm":
                $meta_title = "Фильтры для воды высотой 310 мм в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 310 мм";
                $meta_keywords = "фильтр, вода, высота, 310 мм";
                break;
            case "/catalog/filtry-dlya-vody-vysotoy-570mm":
                $meta_title = "Фильтры для воды высотой 570 мм – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры высотой 570 мм";
                $meta_keywords = "фильтр, вода, высота, 570 мм";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaLength($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-dlya-vody-dlinoy-26cm":
                $meta_title = "Фильтры для воды длиной 26 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры длиной 26 см";
                $meta_keywords = "фильтр, вода, длина, 26 см";
                break;
            case "/catalog/filtry-dlya-vody-dlinoy-30cm":
                $meta_title = "Фильтры для воды длиной 30 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры длиной 30 см";
                $meta_keywords = "фильтр, вода, длина, 30 см";
                break;
            case "/catalog/filtry-dlya-vody-dlinoy-40cm":
                $meta_title = "Фильтры для воды длиной 40 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры длиной 40 см";
                $meta_keywords = "фильтр, вода, длина, 40 см";
                break;
			 case "/catalog/filtry-dlya-vody-dlinoy-50cm":
                $meta_title = "Фильтры для воды длиной 50 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры длиной 50 см";
                $meta_keywords = "фильтр, вода, длина, 50 см";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaWidth($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-dlya-vody-shirinoy-55cm":
                $meta_title = "Фильтры для воды шириной 55 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры шириной 55 см";
                $meta_keywords = "фильтр, вода, ширина, 55 см";
                break;
            case "/catalog/filtry-dlya-vody-shirinoy-60cm":
                $meta_title = "Фильтры для воды шириной 60 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры шириной 60 см";
                $meta_keywords = "фильтр, вода, ширина, 60 см";
                break;
            case "/catalog/filtry-dlya-vody-shirinoy-65cm":
                $meta_title = "Фильтры для воды шириной 65 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры шириной 65 см";
                $meta_keywords = "фильтр, вода, ширина, 65 см";
                break;
            case "/catalog/filtry-dlya-vody-shirinoy-80cm":
                $meta_title = "Фильтры для воды шириной 80 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры шириной 80 см";
                $meta_keywords = "фильтр, вода, ширина, 80 см";
                break;
            case "/catalog/filtry-dlya-vody-shirinoy-110cm":
                $meta_title = "Фильтры для воды шириной 110 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры шириной 110 см";
                $meta_keywords = "фильтр, вода, ширина, 110 см";
                break;
            case "/catalog/filtry-dlya-vody-shirinoy-120cm":
                $meta_title = "Фильтры для воды шириной 120 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры шириной 120 см";
                $meta_keywords = "фильтр, вода, ширина, 120 см";
                break;
            case "/catalog/filtry-dlya-vody-shirinoy-150cm":
                $meta_title = "Фильтры для воды шириной 150 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры шириной 150 см";
                $meta_keywords = "фильтр, вода, ширина, 150 см";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaDiameter($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-dlya-vody-diametrom-21cm":
                $meta_title = "Фильтры для воды диаметром 21 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры диаметром 21 см";
                $meta_keywords = "фильтр, вода, диаметр, 21 см";
                break;
            case "/catalog/filtry-dlya-vody-diametrom-26cm":
                $meta_title = "Фильтры для воды диаметром 26 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры диаметром 26 см";
                $meta_keywords = "фильтр, вода, диаметр, 26 см";
                break;
            case "/catalog/filtry-dlya-vody-diametrom-33cm":
                $meta_title = "Фильтры для воды диаметром 33 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры диаметром 33 см";
                $meta_keywords = "фильтр, вода, диаметр, 33 см";
                break;
            case "/catalog/filtry-dlya-vody-diametrom-37cm":
                $meta_title = "Фильтры для воды диаметром 37 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры диаметром 37 см";
                $meta_keywords = "фильтр, вода, диаметр, 37 см";
                break;
            case "/catalog/filtry-dlya-vody-diametrom-41cm":
                $meta_title = "Фильтры для воды диаметром 41 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры диаметром 41 см";
                $meta_keywords = "фильтр, вода, диаметр, 41 см";
                break;
            case "/catalog/filtry-dlya-vody-diametrom-130mm":
                $meta_title = "Фильтры для воды диаметром 130 мм: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры диаметром 130 мм";
                $meta_keywords = "фильтр, вода, диаметр, 130 мм";
                break;
            case "/catalog/filtry-dlya-vody-diametrom-140mm":
                $meta_title = "Фильтры для воды диаметром 140 мм – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры диаметром 140 мм";
                $meta_keywords = "фильтр, вода, диаметр, 140 мм";
                break;
            case "/catalog/filtry-dlya-vody-diametrom-170mm":
                $meta_title = "Фильтры для воды диаметром 170 мм: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В каталоге интернет-магазина Уютный Дом вы найдете фильтры диаметром 170 мм";
                $meta_keywords = "фильтр, вода, диаметр, 170 мм";
                break;
        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

    private function getMetaDimensions($uri, $desc_template, $keyw_template)
    {
        switch ($uri) {
            case "/catalog/filtry-dlya-vody-gabarity-140x158mm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 140х158 мм – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 140х158 мм";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 140х158 мм";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-158x180mm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 158х180 мм: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 158х180 мм";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 158х180 мм";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-170x449mm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 170x449 мм: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 170x449 мм";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 170x449 мм";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-179x180mm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 179х180 мм – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 179х180 мм";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 179х180 мм";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-20,8x59,5cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 20,8х59,5 см – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 20,8х59,5 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 20,8х59,5 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-20,8x76,2cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 20,8х76,2 см: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 20,8х76,2 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 20,8х76,2 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-24x8x8cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 24х8х8 см в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 24х8х8 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 24х8х8 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-28x8x8cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 28x8x8 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 28x8x8 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 28x8x8 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-29.2x55x47cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 29.2х55х47 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 29.2х55х47 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 29.2х55х47 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-29x55x47cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 29х55х47 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 29х55х47 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 29х55х47 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-38x77,5x66cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 38х77,5х66 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 38х77,5х66 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 38х77,5х66 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-56x8x8cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 56х8х8 см: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 56х8х8 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 56х8х8 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-57x125cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 57х125 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 57х125 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 57х125 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-57x129cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 57х129 см – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 57х129 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 57х129 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-62x139cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 62х139 см – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 62х139 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 62х139 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-62x150cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 62х150 см: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 62х150 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 62х150 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-62x154cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 62х154 см в Санкт-Петербурге и Москве: описание и цена в интернет-магазине Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 62х154 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 62х154 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-69x154cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 69х154 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 69х154 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 69х154 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-70x45x16":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 70х45х16: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 70х45х16";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 70х45х16";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-70x70x16":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 70х70х16: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 70х70х16";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 70х70х16";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-78x29x70cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 78х29х70 см – купить бытовой фильтр в интернет-магазине Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 78х29х70 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 78х29х70 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-78x45x29":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 78х45х29: стоимость фильтра в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 78х45х29";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 78х45х29";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-81x153cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 81х153 см: цена, описание – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 81х153 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 81х153 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-86x180cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 86х180 см – продажа фильтров в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 86х180 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 86х180 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-90x17x9cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 90x17x9 см – заказать фильтр в Санкт-Петербурге и Москве – интернет-магазин Уютный дом";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 90x17x9 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 90x17x9 см";
                break;
            case "/catalog/filtry-dlya-vody-gabarity-93x171cm":
                $meta_title = "Фильтры для воды с габаритами (ШxВxГ) 93х171 см: цена, фото – интернет-магазин Уютный дом в Санкт-Петербурге и Москве";
                $meta_description = "В интернет-магазине Уютный Дом представлены фильтры с габаритными размерами (ШхВхГ) 93х171 см";
                $meta_keywords = "фильтр, вода, габарит, размер, ш, в, г, 93х171 см";
                break;

        }

        return array(
            "meta_title" => $meta_title,
            "meta_description" => $meta_description,
            "meta_keywords" => $meta_keywords, "pageName" => $pageName
        );
    }

}

?>