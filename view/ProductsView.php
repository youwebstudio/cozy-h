<?PHP

/**
 * Simpla CMS
 *
 * @copyright    2011 Denis Pikusov
 * @link        http://simplacms.ru
 * @author        Denis Pikusov
 *
 * Этот класс использует шаблон products.tpl
 *
 */

require_once('View.php');

class ProductsView extends View
{
    /**
     *
     * Отображение списка товаров
     *
     */
    function fetch()
    {
        $filter = array();
        $filter['visible'] = 1;
        // GET-Параметры
        $_filter = $this->request->get('filter', 'boolean');

        if($_filter){
            $this->design->assign('filter_data', json_encode($_GET));

            $categories = $this->request->get('category');
            $brands = $this->request->get('brand');
            $features = $this->request->get('feature');
            $price = $this->request->get('price');

            $filter['brand_id'] = $brands;
            $filter['category_id'] = $categories;
            $filter['features'] = $features;

            $filter['prices']["start"] = $price[0];
            $filter['prices']["finish"] = $price[1];
        } else {
            $category_url = $this->request->get('category', 'string');
            $brand_url = $this->request->get('brand', 'string');
            // Если задан бренд, выберем его из базы
            if (!empty($brand_url)) {
                $brand = $this->brands->get_brand((string)$brand_url);
                if (empty($brand))
                    return false;
                $this->design->assign('brand', $brand);
                $filter['brand_id'] = $brand->id;
            }

            // Выберем текущую категорию
            if (!empty($category_url)) {
                $category = $this->categories->get_category((string)$category_url);
                if (empty($category) || (!$category->visible && empty($_SESSION['admin'])))
                    return false;
                $this->design->assign('category', $category);
                $filter['category_id'] = $category->children;
            }

            // yw
            $tmp = $_SERVER["REQUEST_URI"];
            $tmp = str_replace('/price/filtry-v-cenovom-diapazone-', '', $tmp);
            $tmp = str_replace('-rubley', '', $tmp);
            $arPrice = explode('-', $tmp);
            if (count($arPrice) > 1) {
                $startPrice = $arPrice[0];
                $finishPrice = $arPrice[1];
            }

            if ($this->request->get('option_dynamic', 'string') == "roubles") {
                $filter['prices']["start"] = $startPrice;
                $filter['prices']["finish"] = $finishPrice;
            }
            //print_r($filter);
// 		 print_r($this->products);
            // /////////////////////////////////////////////////////////////////////////////////
        }


        // Если задано ключевое слово
        $keyword = $this->request->get('keyword');
        if (!empty($keyword)) {
            $this->design->assign('keyword', $keyword);
            $filter['keyword'] = $keyword;
        }

        // Сортировка товаров, сохраняем в сесси, чтобы текущая сортировка оставалась для всего сайта
        if ($sort = $this->request->get('sort', 'string'))
            $_SESSION['sort'] = $sort;
        if (!empty($_SESSION['sort']))
            $filter['sort'] = $_SESSION['sort'];
        else
            $filter['sort'] = 'position';
        $this->design->assign('sort', $filter['sort']);

        // Свойства товаров
        if (!empty($category)) {
            $features = array();
            foreach ($this->features->get_features(array('category_id' => $category->id, 'in_filter' => 1)) as $feature) {
                $features[$feature->id] = $feature;
                if (($val = strval($this->request->get($feature->id))) != '')
                    $filter['features'][$feature->id] = $val;
            }

            $options_filter['visible'] = 1;

            $features_ids = array_keys($features);
            if (!empty($features_ids))
                $options_filter['feature_id'] = $features_ids;
            $options_filter['category_id'] = $category->children;
            if (isset($filter['features']))
                $options_filter['features'] = $filter['features'];
            if (!empty($brand))
                $options_filter['brand_id'] = $brand->id;

            $options = $this->features->get_options($options_filter);

            foreach ($options as $option) {
                if (isset($features[$option->feature_id]))
                    $features[$option->feature_id]->options[] = $option;
            }

            foreach ($features as $i => &$feature) {
                if (empty($feature->options))
                    unset($features[$i]);
            }

            $this->design->assign('features', $features);
        }

        require_once($_SERVER['DOCUMENT_ROOT'] . '/api/Filter.php');
        $custom_filter = new Filter();
        $filter_option = $this->request->get('option_dynamic', 'string');
        $caption = $custom_filter->getCaptionByCode($filter_option);

        // фильтрация по опции
        if ($caption !== false) {
            $res_feature = "";

            foreach ($this->features->get_features() as $feature) {
                if (trim($feature->name) == $caption) {
                    $res_feature = $feature;
                    break;
                }
            }
            $res_filter = $custom_filter->getOptionByUri(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), array("option" => $filter_option));

//            print_r(array("option" => $filter_option));
            if ($res_filter) {
                $filter['features'][$res_feature->id] = $res_filter;
            }
        }


        // Постраничная навигация
        $items_per_page = $this->settings->products_num;
        // Текущая страница в постраничном выводе
        $current_page = $this->request->get('page', 'int');
        // Если не задана, то равна 1
        $current_page = max(1, $current_page);
        $this->design->assign('current_page_num', $current_page);
        // Вычисляем количество страниц
        $products_count = $this->products->count_products($filter);

        $page_n = $current_page;

        // Показать все страницы сразу
        if ($this->request->get('page') == 'all') {
            $items_per_page = $products_count;
            $page_n = 'all';
        }

        $pages_num = ceil($products_count / $items_per_page);
        $this->design->assign('total_pages_num', $pages_num);
        $this->design->assign('page_n', $page_n);

        $filter['page'] = $current_page;
        $filter['limit'] = $items_per_page;

        ///////////////////////////////////////////////
        // Постраничная навигация END
        ///////////////////////////////////////////////


        $discount = 0;
        if (isset($_SESSION['user_id']) && $user = $this->users->get_user(intval($_SESSION['user_id'])))
            $discount = $user->discount;

        // Товары
        $products = array();
        foreach ($this->products->get_products($filter) as $p)
            $products[$p->id] = $p;
//        print_r($filter);
        // Если искали товар и найден ровно один - перенаправляем на него
        if (!empty($keyword) && $products_count == 1)
            header('Location: ' . $this->config->root_url . '/products/' . $p->url);

        if (!empty($products)) {
            $products_ids = array_keys($products);
            foreach ($products as &$product) {
                $product->variants = array();
                $product->images = array();
                $product->properties = array();
            }

            $variants = $this->variants->get_variants(array('product_id' => $products_ids, 'in_stock' => true));

            foreach ($variants as &$variant) {
                //$variant->price *= (100-$discount)/100;
                $products[$variant->product_id]->variants[] = $variant;
            }

            $images = $this->products->get_images(array('product_id' => $products_ids));
            foreach ($images as $image)
                $products[$image->product_id]->images[] = $image;

            foreach ($products as &$product) {
                if (isset($product->variants[0]))
                    $product->variant = $product->variants[0];
                if (isset($product->images[0]))
                    $product->image = $product->images[0];

                //$product->rating = rand(35, 50) / 10;
                $product->rating = 4.3;
            }


            /*
            $properties = $this->features->get_options(array('product_id'=>$products_ids));
            foreach($properties as $property)
                $products[$property->product_id]->options[] = $property;
            */

            $this->design->assign('products', $products);
        }

        // Выбираем бренды, они нужны нам в шаблоне
        if (!empty($category)) {
            $brands = $this->brands->get_brands(array('category_id' => $category->children));
            $category->brands = $brands;
        }

        $names = array("water", "material", "disinfection", "ecoferox", "ecotar", "volume", "volume_water", "volume_column", "separate", "pump", "power", "size", "size_connection", "salt", "reset", "speed", "clean_type", "clean_steps", "height", "length", "width", "diameter", "dimensions");
        // Устанавливаем мета-теги в зависимости от запроса
        if ($this->page) {
            $this->design->assign('meta_title', $this->page->meta_title);
            $this->design->assign('meta_keywords', $this->page->meta_keywords);
            $this->design->assign('meta_description', $this->page->meta_description);
        } elseif ($this->request->get('option_dynamic', 'string') == "roubles") {
            $resMeta = $custom_filter->getMeta(array(
                "type" => $this->request->get('option_dynamic', 'string'),
                "startPrice" => $startPrice,
                "finishPrice" => $finishPrice
            ));
        } elseif ($this->request->get('option_dynamic', 'string') == "temperature") {
            $resMeta = $custom_filter->getMeta(array(
                "type" => $this->request->get('option_dynamic', 'string'),
                "startPrice" => $startPrice,
                "finishPrice" => $finishPrice,
                "keyw_template" => "фильтры, вода, температура, максимальная, "
            ));
        } elseif ($this->request->get('option_dynamic', 'string') == "pressure") {
            $resMeta = $custom_filter->getMeta(array(
                "type" => $this->request->get('option_dynamic', 'string'),
                "startPrice" => $startPrice,
                "finishPrice" => $finishPrice,
                "desc_template" => "В интернет-магазине Уютный Дом вы можете подобрать фильтры по максимальному давлению воды. Здесь представлены фильтры с давлением ",
                "keyw_template" => "фильтры, вода, давление, максимальное, "
            ));
        } elseif (in_array($this->request->get('option_dynamic', 'string'), $names)) {
            $resMeta = $custom_filter->getMeta(array(
                "type" => $this->request->get('option_dynamic', 'string')
            ));
        } elseif (isset($category)) {
            $this->design->assign('meta_title', $category->meta_title);
            $this->design->assign('meta_keywords', $category->meta_keywords);
            $this->design->assign('meta_description', $category->meta_description);
        } elseif (isset($brand)) {
            $this->design->assign('meta_title', $brand->meta_title);
            $this->design->assign('meta_keywords', $brand->meta_keywords);
            $this->design->assign('meta_description', $brand->meta_description);
        } elseif (isset($keyword)) {
            $this->design->assign('meta_title', $keyword);
        }

        if (!empty($resMeta)) {
            $meta_title = $resMeta['meta_title'];
            $meta_keywords = $resMeta['meta_keywords'];
            $meta_description = $resMeta['meta_description'];
            $pageName = $resMeta['pageName'];
            $this->design->assign('meta_title', $meta_title);
            $this->design->assign('meta_keywords', $meta_keywords);
            $this->design->assign('meta_description', $meta_description);
//            $this->design->assign('h1', '222');
//            $this->design->assign('name', '111');
//            $this->design->assign('page', '111');
//            $this->design->assign('keywords', '111');
            $this->design->assign('pageName', $pageName);
//            print_r($resMeta);
        }

        $this->body = $this->design->fetch('products.tpl');
        return $this->body;
    }


}
