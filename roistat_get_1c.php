<?php
header('Content-Type: text/json; charset=utf-8');
ini_set("soap.wsdl_cache_enabled", "0" ); 

try {
  $client = new SoapClient(
      "https://gateway.scloud.ru/ecvols_base01/ws/RoistatExchange?wsdl",
      array(
          'login' => 'ecvols4roistat',
          'password' => 'Roistat27',
          'soap_version' => SOAP_1_2,
          'features' => SOAP_USE_XSI_ARRAY_TYPE, 
          'trace' => true,
      )
  );


  $data = array(
    'user' => $_REQUEST['user'],
    'token' => $_REQUEST['token'],    
    'date' => $_REQUEST['date'],
    'action' => $_REQUEST['action'],
    'limit' => $_REQUEST['limit'],
    'offset' => $_REQUEST['offset'],
  );

  $result = $client->UploadData($data);
  if(isset($result->return)){
    echo $result->return;
    
  }

} catch (SoapFault $e) {
  var_dump(constructErrorResponse($e->getMessage()));
}


function constructErrorResponse($message)
{
    $response['error'] = 1;
    $response['message'] = $message;
    return $response;
}
?>